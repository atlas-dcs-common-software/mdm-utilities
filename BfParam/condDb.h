//
//  condDb.h ------
//
#ifndef CONDDB_H
#define CONDDB_H

#ifndef CONFDB_DUMMY

#include <occi.h>
#include <string>

using namespace oracle::occi;

class condDb
{
public:
                   condDb(const std::string& sIniFile);
                  ~condDb();

   bool            getStatus()   { return _bStatus;   };
   std::string     getError()    { return _sError;    };
   std::string     getDatabase() { return _sDatabase; };
   std::string     getUsername() { return _sUsername; };
   std::string     getPassword() { return _sPassword; };
   std::string     getTable()    { return _sTable;    };

   bool            storeParams(const std::string& sSensorId, const int dBgain, const int dTgain,
                                    const int dOffset1, const int dOffset2, const int dOffset3, const int dOffset4);
   bool            showSensor(const std::string& sSensorId);
   bool            wrIniUsedFile(const std::string& sIniFile);

private:
   bool            _bStatus;
   std::string     _sError;

   std::string     _sDatabase;
   std::string     _sUsername;
   std::string     _sPassword;
   std::string     _sTable;
        
   Environment*    _pEnv;
   Connection*     _pCon;
};


#else // CONFDB_DUMMY

#include <string>
class condDb
{
public:
                   condDb( const std::string& ) { };
                  ~condDb() { };

   bool            getStatus()   { return false;   };
   std::string     getError()    { return std::string("<condDb: dummy class>"); };
   std::string     getDatabase() { return std::string(""); };
   std::string     getUsername() { return std::string(""); };
   std::string     getPassword() { return std::string(""); };
   std::string     getTable()    { return std::string(""); };

   bool            storeParams( const std::string&, const int, const int,
                                const int, const int, const int, const int ) { return false; }
   bool            showSensor( const std::string& ) { return false; }
   bool            wrIniUsedFile( const std::string& ) { return false; }
};

#endif // CONFDB_DUMMY

#endif // CONDDB_H
