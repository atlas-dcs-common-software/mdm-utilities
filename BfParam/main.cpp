/*
 *	main ------
 *	Starting point of the BfParam application.
 *	July 2015, Robert.Hart@nikhef.nl
 */

#include <QApplication>

#include "BfParamDialog.h"

int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );

  BfParamDialog bfparam;
  bfparam.show();
  return app.exec();
}
