/*
 *  BfParam.h ------
 */

#ifndef BFPARAMDIALOG_H
#define BFPARAMDIALOG_H

#include <QDialog>

#include "ui_BfParam.h"

/*
#ifndef WIN32
#error WIN32 not set!
#endif

#ifndef WIN64
#error WIN64 not set!
#endif
*/

// Henk: huh? what's this?:
//#ifdef UNICODE
//#error UNICODE set!
//#endif

// forward declaration
class kernel;

class BfParamDialog: public QDialog, public Ui_Dialog
{
    Q_OBJECT

public:
                BfParamDialog();
               ~BfParamDialog();

private:
    kernel*     _pKernel;
    int         _dInterfaceType;

    void        cleanUp();
    void        readAppSettings();
    void        writeAppSettings();

private slots:
    void        onQuit();
    void        onOpen();
    void        onScan();
    void        onScanNext();
    void        onScanAll();
    void        onToDbase();
    void        onVerbose();
    void        onLogfile();
    void        onInterface(const QString &text);

public slots:
    void        upd_Stdout(const QString& text);
    void        upd_OpenCloseStatus(const bool bOpen);
    void        upd_WindowTitle(const QString& sTitle);
    void        upd_PortNumber(const int iPort);
    void        upd_EndScanAll();

signals:
    void        sigOpenClose(const QString& sInterfaceType, const int dInterfacetype, const int iPortNo, const int iBaudrate);
    void        sigScan();
    void        sigScanNext();
    void        sigScanAll();
    void        sigToDbase(const bool bToDbase);
    void        sigVerbose(const bool bVerbose);
    void        sigLogfile(const bool bLogfile);
    void        sigPortCount(const int iPortCount);

};

#endif
