/*
 *  kernel.h ------
 */

#ifndef KERNEL_H
#define KERNEL_H

#include <QThread>
#include <QMessageBox>

// forward declarations
class BfParamDialog;
class CanInterface;
class CanNode;
class confDb;
class condDb;

class kernel: public QThread
{
    Q_OBJECT

public:
                    kernel(BfParamDialog* pDialog);
                   ~kernel();

    void            run();
    bool            initKernel();
    bool            getStatus()  { return _bStatus; }

private:
    bool            _bStatus;
    BfParamDialog*  _pBfParamDialog;
    CanInterface*   _pCanInterface;
    CanNode*        _pCanNode;
    confDb*         _pConfDb;
    condDb*         _pCondDb;
    bool            _bConfDbStat;
    bool            _bCondDbStat;
    QString         _sInterfaceType;
    int             _dInterfaceType;
    std::string     _sHostname;
    int             _iPortCount;
    int             _iPortNo;
    int             _iBaudrate;
    bool            _bToDbase;
    bool            _bVerbose;
    bool            _bLogfile;

    void            cleanUp();
    void            setWindowTitle();
    void            wrIniUsedFile(const std::string& sIniFile);

    void            openCloseCanPort(const bool bReportError,   const QString& sInterfaceType,
                                     const int  dInterfaceType, const int iPortNo, const int iBaudrate,
                                     const bool bNext);
    std::string     convertSerial(const int iSerial);
    void            doScan();
    void            doScanNext();
    void            doScanAll();
    void            log(const QString& sInfo);
    void            pSettingError(const QString& sInfo);
    void            pSeriousError(const QString& sInfo);

    bool            getBmask(const int iNode, int& dBmask);
    bool            getBsensorId(const int iNode, const int dSensor, std::string& sId);
    bool            getSensorOffset(const int iNode, const int dSensor,
                        int& dOffsetH1, int& dOffsetH2, int& dOffsetH3, int& dOffsetTemp);
    bool            getSensorGain(const int iNode, const int dSensor,
                          int& dGainH1, int& dGainH2, int& dGainH3, int& dGainTemp);
    bool            readSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                          const unsigned int iTimeout, int &dResult);

private slots:

public slots:
    void            upd_OpenClose(const QString& sInterfaceType, const int dInterfaceType, const int iPortNo, const int iBaudrate);
    void            upd_Scan();
    void            upd_ScanNext();
    void            upd_ScanAll();
    void            upd_ToDbase(const bool bToDbase);
    void            upd_Verbose(const bool bVerbose);
    void            upd_Logfile(const bool bLogfile);
    void            upd_PortCount(const int iPortCount);

signals:
    void            sigLogEdit(const QString& sMess);
    void            sigOpenCloseStatus(const bool bOpen);
    void            sigWindowTitle(const QString& sTitle);
    void            sigPortNumber(const int iPort);
    void            sigEndScanAll();
};

#endif
