//
//      condDb.cpp ------
//
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include "condDb.h"
#include "configFile.h"

#define BUF_SIZE            1024

#define CONDDB_SET          "CondDb"
#define CONDDB_USERNAME     "CondDbUser"
#define CONDDB_PASSWORD     "CondDbPwd"
#define CONDDB_DATABASE     "CondDbDbase"
#define CONDDB_TABLE        "CondDbTable"

// default CondDb parameters
#define CONDDB_D_USERNAME   "ATLAS_MDT_DCS_W"
#define CONDDB_D_PASSWORD   "atl_mdt_dcs_w"
#define CONDDB_D_DATABASE   "ATONR_MDT_DCS"
#define CONDDB_D_TABLE      "BSENSOR_CALIB"

condDb::condDb(const std::string& sIniFile)
{
  _bStatus = false;
  _sError  = "";
        
  configFile*   pConfig  = new configFile();

  _sDatabase = pConfig->getStringVal(sIniFile, std::string(CONDDB_DATABASE),
                                     std::string(CONDDB_D_DATABASE));
  _sUsername = pConfig->getStringVal(sIniFile, std::string(CONDDB_USERNAME),
                                     std::string(CONDDB_D_USERNAME));
  _sPassword = pConfig->getStringVal(sIniFile, std::string(CONDDB_PASSWORD),
                                     std::string(CONDDB_D_PASSWORD));
  _sTable    = pConfig->getStringVal(sIniFile, std::string(CONDDB_TABLE),
                                     std::string(CONDDB_D_TABLE));

  _pEnv = Environment::createEnvironment(Environment::DEFAULT);

  try
    {
      // Open a connection to the database
      _pCon    = _pEnv->createConnection( _sUsername, _sPassword, _sDatabase );
      _bStatus = true;
    }
  catch( SQLException &ea )
    {
      _sError  = "what() says: ";
      _sError += ea.what();
      _bStatus = false;
    }
}

condDb::~condDb()
{
  if (_bStatus == true)
    _pEnv->terminateConnection( _pCon );
  Environment::terminateEnvironment( _pEnv );
}

bool condDb::wrIniUsedFile(const std::string& sIniFile)
{
  FILE  *fp;
  fp = fopen(sIniFile.c_str(), "a+");
  if (fp == NULL)
    return false;

  fprintf(fp, "[%s]\n",      CONDDB_SET);
  fprintf(fp, "%-14s= %s\n", CONDDB_DATABASE, _sDatabase.c_str());
  fprintf(fp, "%-14s= %s\n", CONDDB_USERNAME, _sUsername.c_str());
  fprintf(fp, "%-14s= %s\n", CONDDB_PASSWORD, _sPassword.c_str());
  fprintf(fp, "%-14s= %s\n", CONDDB_TABLE,    _sTable.c_str());
  fprintf(fp, "\n");

  fclose(fp);
  return true;
}

bool condDb::storeParams(const std::string& sSensorId, const int dBgain, const int dTgain,
                         const int dOffset1, const int dOffset2, const int dOffset3, const int dOffset4)
{
  char cBuf[BUF_SIZE];
  time_t clock;
  time(&clock);

  std::string   sCvId;
  sCvId  = "'";
  sCvId += sSensorId;
  sCvId += "',";

  struct        tm*     tInfo = localtime(&clock);
  (void)strftime(cBuf, BUF_SIZE, "'%d %B %Y %I:%M:%S %p',", tInfo);
  std::string   sTime = cBuf;

  sprintf(cBuf, "%d,%d,%d,%d,%d,%d)", dBgain, dTgain, dOffset1, dOffset2, dOffset3, dOffset4);
  std::string   sVariables = cBuf;

  std::string   sCmdSQL;
  sCmdSQL  = "INSERT INTO ";
  sCmdSQL += _sTable;
  sCmdSQL += "(STIME,SENSOR_ID,B_GAIN,T_GAIN,OFFSET_1,OFFSET_2,OFFSET_3,OFFSET_4) ";
  sCmdSQL += "VALUES(";
  sCmdSQL += sTime;
  sCmdSQL += sCvId;
  sCmdSQL += sVariables;

  // std::cout << sCmdSQL << std::endl;

  bool  bResult = true;

  try
    {
      Statement *s;

      s = _pCon->createStatement(sCmdSQL.c_str());
      (void)s->executeQuery();
    }
  catch( SQLException &ea )
    {
      _sError  = "what() says: ";
      _sError += ea.what();
      _bStatus = false;
    }

  return bResult;
}

bool condDb::showSensor(const std::string& sSensorId)
{
  bool        bResult;
  std::string sCmdSQL;

  sCmdSQL  = "SELECT * FROM ";
  sCmdSQL += _sTable;
  sCmdSQL += " WHERE SENSOR_ID = '";
  sCmdSQL += sSensorId;
  sCmdSQL += "' ORDER BY STIME";

  std::cout << sCmdSQL << std::endl;

  try
    {
      Statement *s;
      ResultSet *r;

      s = _pCon->createStatement(sCmdSQL.c_str());
      r = s->executeQuery();

      int       n = 0;

      while(r->next() == ResultSet::DATA_AVAILABLE)
        {
          ++n;
          std::string   s1 = r->getString(1);
          std::string   s2 = r->getString(2);
          std::string   s3 = r->getString(3);
          std::string   s4 = r->getString(4);
          std::string   s5 = r->getString(5);
          std::string   s6 = r->getString(6);
          std::string   s7 = r->getString(7);
          std::string   s8 = r->getString(8);

          std::cout << s1 << ", " << s2 << ", " << s3 << ", " << s4 << ", " << s5 << ", " << s6 << ", " << s7 << ", " << s8 << std::endl;
        }
      std::cout << "#entries: " << n << std::endl;
      bResult = true;
    }
  catch( SQLException &ea )
    {
      _sError = "what() says: ";
      _sError += ea.what();
      bResult = false;
    }
  return bResult;
}
