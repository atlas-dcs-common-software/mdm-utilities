/*
 *  kernel.cpp ------
 *  The heart of the BfParam application.
 */

#include <iostream>
#include <cstdio>
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "kernel.h"
#include "BfParamDialog.h"
#include "CanInterface.h"
#include "CanNode.h"
#include "confDb.h"
#include "condDb.h"
#include "configFile.h"
#include "mdt_obj_dict.h"

#define BFPARAM_INI_FILE    "./bfparam.ini"
#define SETTING_SECTION     "BfParam"

#define BUF_SIZE            1024
#define MAX_BSENSOR         4

const  QString gsVersion( "v4.1" );

extern bool   gbStopThread;
extern bool   gbScanAll;

// Constructor
kernel::kernel(BfParamDialog* pDialog)
  : _pCanInterface(0)
{
  _bStatus        = false;
  _pBfParamDialog = pDialog;
  _pCanInterface  = NULL;
  _pCanNode       = NULL;
  _pConfDb        = NULL;
  _pCondDb        = NULL;
  _sHostname      = "?";
  _dInterfaceType = -1;
  _iPortCount     = -1;
  _iPortNo        = -1;
  _iBaudrate      = -1;

  connect(_pBfParamDialog,
          SIGNAL(sigOpenClose(const QString&, const int, const int, const int)),
          this,
          SLOT(upd_OpenClose(const QString&, const int, const int, const int)));

  connect(_pBfParamDialog, SIGNAL(sigScan()),               this, SLOT(upd_Scan()));
  connect(_pBfParamDialog, SIGNAL(sigScanNext()),           this, SLOT(upd_ScanNext()));
  connect(_pBfParamDialog, SIGNAL(sigScanAll()),            this, SLOT(upd_ScanAll()));
  connect(_pBfParamDialog, SIGNAL(sigToDbase(const bool)),  this, SLOT(upd_ToDbase(const bool)));
  connect(_pBfParamDialog, SIGNAL(sigVerbose(const bool)),  this, SLOT(upd_Verbose(const bool)));
  connect(_pBfParamDialog, SIGNAL(sigLogfile(const bool)),  this, SLOT(upd_Logfile(const bool)));
  connect(_pBfParamDialog, SIGNAL(sigPortCount(const int)), this, SLOT(upd_PortCount(const int)));
}

// Destructor
kernel::~kernel()
{
  this->cleanUp();
}

void kernel::cleanUp()
{
  if (_pCondDb)
    delete _pCondDb;
  if (_pConfDb)
    delete _pConfDb;
  if (_pCanNode)
    delete _pCanNode;
  if (_pCanInterface)
    delete _pCanInterface;

  _pCanInterface = NULL;
  _pCanNode      = NULL;
  _pConfDb       = NULL;
  _pCondDb       = NULL;
}

bool kernel::initKernel()
{
  if (_bStatus == true)
    return true;

  std::string sIniFile = BFPARAM_INI_FILE;

  char host[BUF_SIZE];
  if (gethostname(host, sizeof(host)) == 0)
    _sHostname = host;

  _pConfDb = new confDb(sIniFile);
  _bConfDbStat = _pConfDb->getStatus();
  if (_bConfDbStat == false)
    {
      QString sError;
      sError = "WARNING: confDb constructor failed";
      log(sError);
      sError = _pConfDb->getError().c_str();
      log(sError);
    }
  //_pConfDb->showCanbusList();

  _pCondDb = new condDb(sIniFile);
  _bCondDbStat = _pCondDb->getStatus();
  if (_bCondDbStat == false)
    {
      QString sError;
      sError = "WARNING: condDb constructor failed";
      log(sError);
      sError = _pCondDb->getError().c_str();
      log(sError);
    }

  wrIniUsedFile(sIniFile);

  setWindowTitle();

  QString sInfo;
  sInfo = QString::asprintf("On host: %s", _sHostname.c_str());
  log(sInfo);
  _bStatus = true;
  return _bStatus;
}

void kernel::run()
{
  if (_bStatus == false)
    return;

  while(gbStopThread == false)
    {
      sleep(1);
      qApp->processEvents();
    }
}

void kernel::upd_OpenClose( const QString& sInterfaceType,
                            const int      dInterfaceType,
                            const int      iPortNo,
                            const int      iBaudrate )
{
  _sInterfaceType = sInterfaceType;
  _dInterfaceType = dInterfaceType;
  _iPortNo        = iPortNo;
  _iBaudrate      = iBaudrate;

  openCloseCanPort(true, sInterfaceType, dInterfaceType, iPortNo, iBaudrate, false);
}

void kernel::upd_Scan()
{
  doScan();
}

void kernel::upd_ScanNext()
{
  doScanNext();
}

void kernel::upd_ScanAll()
{
  doScanAll();
}

void kernel::upd_ToDbase(const bool bToDbase)
{
  _bToDbase = bToDbase;
}

void kernel::upd_Verbose(const bool bVerbose)
{
  _bVerbose = bVerbose;
}

void kernel::upd_Logfile(const bool bLogfile)
{
  _bLogfile = bLogfile;
}

void kernel::upd_PortCount(const int iPortCount)
{
  _iPortCount = iPortCount;
}

void kernel::openCloseCanPort( const bool bReportError,
                               const QString& sInterfaceType, const int dInterfaceType,
                               const int iPortNo, const int iBaudrate, const bool bNext )
{
  QString sError;

  if (_pCanInterface)
    {
      // close/delete the Can Node and Interface port object
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = NULL;
      delete _pCanInterface;
      _pCanInterface = NULL;
      if (bNext == false)
        {
          emit sigOpenCloseStatus(true);
          return;
        }
    }

  _pCanInterface = CanInterface::newCanInterface(dInterfaceType, iPortNo, iBaudrate);

  if (_pCanInterface)
    {
      if (_pCanInterface->opStatus() != 0)
        {
          if (bReportError == true)
            {
              sError  = sInterfaceType;
              sError += " CAN port: \n";
              sError += _pCanInterface->errString().c_str();
              log(sError);
            }
          // Delete the interface object
          delete _pCanInterface;
          _pCanInterface = NULL;
          return;
        }

      // CAN port opened successfully
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = new CanNode(_pCanInterface, 0);

      if (bNext == false)
        {
          emit sigOpenCloseStatus(false);
        }

      QString     sInfo, sTmp;
      std::string sCanbus;

      sInfo  = "\nOpen CAN bus (";
      sInfo += sInterfaceType;
      sTmp = QString::asprintf(", %2d, %d)", iPortNo, iBaudrate);
      sInfo += sTmp;
      sTmp = "";
      if (_pConfDb->getCanbus(_sHostname, iPortNo, sCanbus) == true)
        sTmp = QString::asprintf(": %s", sCanbus.c_str());
      sInfo += sTmp;
      log(sInfo);
      return;
    }

  if (bReportError == true)
    {
      sError  = sInterfaceType;
      sError += " CAN port: \n";
      sError += "Failed to create the CAN port object";
      log(sError);
    }
}

void kernel::log(const QString& sMess)
{
  emit sigLogEdit(sMess);

  if (_bLogfile == false)
    return;

  char cBuf[BUF_SIZE];
  time_t clock;
  time(&clock);

  struct tm* tInfo = localtime(&clock);

  sprintf(cBuf, "./bplog_%02d%02d%02d",
          tInfo->tm_year -100, tInfo->tm_mon+1, tInfo->tm_mday);
  std::string sFile = cBuf;

  FILE *fp;
  fp = fopen(sFile.c_str(), "a+");
  if (fp == NULL)
    {
      QString sInfo;
      sInfo = QString::asprintf("Cannot write logfile: %s", sFile.c_str());
      _bLogfile = false;	// no recursion
      emit sigLogEdit(sInfo);
      return;
    }

  std::string sUtf8Mess = sMess.toUtf8().constData();
  fprintf(fp, "%s\n", sUtf8Mess.c_str());
  fclose(fp);
}

void kernel::setWindowTitle()
{
  QString sWindowTitle = " BfParam [" + gsVersion + "]";

  if (_bConfDbStat == true)
    {
      QString sConfDb;
      sConfDb = QString::asprintf(", %s@%s", _pConfDb->getUsername().c_str(),
                                  _pConfDb->getDatabase().c_str());
      sWindowTitle += sConfDb;
    }
  if (_bCondDbStat == true)
    {
      QString sCondDb;
      sCondDb = QString::asprintf(", %s@%s", _pCondDb->getUsername().c_str(),
                                  _pCondDb->getDatabase().c_str());
      sWindowTitle += sCondDb;
    }
  emit sigWindowTitle(sWindowTitle);
}

void kernel::wrIniUsedFile(const std::string& sIniFile)
{
  std::string sIniUsedFile = sIniFile + ".used";

  FILE *fp;
  fp = fopen(sIniUsedFile.c_str(), "w");
  if (fp == NULL)
    return;

  fprintf(fp, "#\n# INI_FILE =  %s\n#\n", BFPARAM_INI_FILE);
  fprintf(fp, "[%s]\n", SETTING_SECTION);
  fprintf(fp, "\n");
  fclose(fp);

  if (_bConfDbStat == true)
    {
      if (_pConfDb->wrIniUsedFile(sIniUsedFile) == false)
        {
          QString sInfo;
          sInfo = QString::asprintf("pConfDb: cannot write ini file %s",
                                    sIniUsedFile.c_str());
          log(sInfo);
        }
    }
  if (_bCondDbStat == true)
    {
      if (_pCondDb->wrIniUsedFile(sIniUsedFile) == false)
        {
          QString sInfo;
          sInfo = QString::asprintf("pCondDb: cannot write ini file %s",
                                    sIniUsedFile.c_str());
          log(sInfo);
        }
    }
}

void kernel::doScan()
{
  QString sInfo;

  int nToDbase = 0;
  int nNodes   = _pCanInterface->scanBus();

  for(int i = 0; i < nNodes; ++i)
    {
      bool        bFound;
      int         dBmask;
      std::string sChamber;

      int         iSerial = _pCanInterface->scannedSerialNr(i);
      std::string sSerial = convertSerial(iSerial);
      int         iNode   = _pCanInterface->scannedNodeId(i);

      if (_pConfDb->getChamber(sSerial, bFound, sChamber) == false)
        {
          sInfo = QString::asprintf("node %3d, getChamber(%s) failed [%s]",
                                    iNode, sSerial.c_str(), _pConfDb->getError().c_str());
          pSeriousError(sInfo);
          continue;
        }
      if (bFound == false)
        {
          sInfo = QString::asprintf("node %3d, %s not in ConfDb",
                                    iNode, sSerial.c_str());
          pSeriousError(sInfo);
          continue;
        }
      if (_pCanNode->setNodeId(iNode) == false)
        {
          sInfo = QString::asprintf("setNodeId( %3d ) failed",
                                    iNode);
          pSeriousError(sInfo);
          continue;
        }
      if (getBmask(iNode, dBmask) == false)
        continue;

      sInfo = QString::asprintf("NODE: %3d, SERIAL: %s, MASK: x%X, CHAMBER: %s",
                                iNode, sSerial.c_str(), dBmask, sChamber.c_str());
      log(sInfo);

      if (dBmask == 0)
        continue;

      std::string sSensorId;
      int         dOffH1,  dOffH2,  dOffH3,  dOffTemp;
      int         dGainH1, dGainH2, dGainH3, dGainTemp;

      for(int dSensor = 0; dSensor < MAX_BSENSOR; ++dSensor)
        {
          int dMask = (1 << dSensor);
          if ((dBmask & dMask) == 0)
            continue;

          if (getBsensorId(iNode, dSensor, sSensorId) == false)
            continue;
          if (_bVerbose == true)
            {
              sInfo = QString::asprintf("     #%d:  %s",
                                        dSensor, sSensorId.c_str());
              log(sInfo);
            }
          if (getSensorOffset(iNode, dSensor, dOffH1, dOffH2, dOffH3, dOffTemp) == false)
            continue;
          if (getSensorGain(iNode, dSensor, dGainH1, dGainH2, dGainH3, dGainTemp) == false)
            continue;
          if (_bVerbose == true)
            {
              sInfo = QString::asprintf("           OFFSET:  H1: x%06X, H2: x%06X, H3: x%06X, T: x%06X",
                                        dOffH1, dOffH2, dOffH3, dOffTemp);
              log(sInfo);
              sInfo = QString::asprintf("           GAIN  :  H1: x%06X, H2: x%06X, H3: x%06X, T: x%06X",
                                        dGainH1, dGainH2, dGainH3, dGainTemp);
              log(sInfo);
            }

          if (_bToDbase == true)
            {
              if (_pCondDb->storeParams(sSensorId, dGainH1, dGainTemp,
                                        dOffH1, dOffH2, dOffH3, dOffTemp) == true)
                ++nToDbase;
              else
                {
                  sInfo = QString::asprintf("storage parameters %s failed",
                                            sSensorId.c_str());
                  pSeriousError(sInfo);
                }
            }
        }
    }

  sInfo = QString::asprintf("\n#nodes: %d, #todbase: %d", nNodes, nToDbase);
  log(sInfo);
}

void kernel::doScanNext()
{
  int iPort = _iPortNo;

  ++iPort;
  if (iPort >= _iPortCount)
    iPort = 0;
  _iPortNo = iPort;

  openCloseCanPort(true, _sInterfaceType, _dInterfaceType, _iPortNo, _iBaudrate, true);
  emit sigPortNumber(_iPortNo);
  doScan();
}

void kernel::doScanAll()
{
  for(int iPort = 0; iPort < _iPortCount; ++iPort)
    {
      if (gbScanAll == false)
        break;
      _iPortNo = iPort;
      openCloseCanPort(true, _sInterfaceType, _dInterfaceType, _iPortNo, _iBaudrate, true);
      emit sigPortNumber(_iPortNo);
      doScan();
      sleep(1);
    }
  emit sigEndScanAll();
}

bool kernel::getBmask(const int iNode, int& dBmask)
{
  return readSdoObject(iNode, OD_BF_NSENSORS, 0, 200, dBmask);
}

bool kernel::getBsensorId(const int iNode, const int dSensor, std::string& sSensorId)
{
  int iIndex;
  int iSubIndex;

  sSensorId = "?";

  switch(dSensor)
    {
    case 0: iIndex = OD_BF_ID0; break;
    case 1: iIndex = OD_BF_ID1; break;
    case 2: iIndex = OD_BF_ID2; break;
    case 3: iIndex = OD_BF_ID3; break;
    default: 
      QString sInfo;
      sInfo = QString::asprintf("getBsensorId( node=%d, sensor=%d ) illegal", iNode, dSensor);
      pSeriousError(sInfo);
      return false;
    }

  unsigned int iTimeout = 400;
  int iIdLow, iIdHigh;

  iSubIndex = 1;
  if (readSdoObject(iNode, iIndex, iSubIndex, iTimeout, iIdLow) == false)
    return false;
  iSubIndex = 2;
  if (readSdoObject(iNode, iIndex, iSubIndex, iTimeout, iIdHigh) == false)
    return false;

  char buf[BUF_SIZE];
  sprintf(buf, "x%08X%08X", iIdHigh, iIdLow);
  sSensorId = buf;
  return true;
}

bool kernel::getSensorOffset( const int iNode, const int dSensor,
                              int& dOffsetH1, int& dOffsetH2, int& dOffsetH3,
                              int& dOffsetTemp )
{
  int iIndex = 0;
  unsigned int iTimeout = 200;

  switch(dSensor)
    {
    case 0: iIndex = OD_BF_ADC_B0; break;
    case 1: iIndex = OD_BF_ADC_B1; break;
    case 2: iIndex = OD_BF_ADC_B2; break;
    case 3: iIndex = OD_BF_ADC_B3; break;
    default: 
      QString  sInfo;
      sInfo = QString::asprintf("getSensorOffset( node=%d, sensor=%d ) illegal",
                                iNode, dSensor);
      pSeriousError(sInfo);
      return false;
    }

  if (readSdoObject(iNode, iIndex, SI_OFFSET_H1,   iTimeout, dOffsetH1)   == false ||
      readSdoObject(iNode, iIndex, SI_OFFSET_H2,   iTimeout, dOffsetH2)   == false ||
      readSdoObject(iNode, iIndex, SI_OFFSET_H3,   iTimeout, dOffsetH3)   == false ||
      readSdoObject(iNode, iIndex, SI_OFFSET_TEMP, iTimeout, dOffsetTemp) == false)
    return false;

  return true;
}

bool kernel::getSensorGain( const int iNode, const int dSensor,
                            int& dGainH1, int& dGainH2, int& dGainH3,
                            int& dGainTemp)
{
  int iIndex = 0;
  unsigned int iTimeout = 200;

  switch(dSensor)
    {
    case 0: iIndex = OD_BF_ADC_B0; break;
    case 1: iIndex = OD_BF_ADC_B1; break;
    case 2: iIndex = OD_BF_ADC_B2; break;
    case 3: iIndex = OD_BF_ADC_B3; break;
    default: 
      QString sInfo;
      sInfo = QString::asprintf("getSensorGain( node=%d, sensor=%d ) illegal",
                                iNode, dSensor);
      pSeriousError(sInfo);
      return false;
    }

  if (readSdoObject(iNode, iIndex, SI_GAIN_H1,   iTimeout, dGainH1)   == false ||
      readSdoObject(iNode, iIndex, SI_GAIN_H2,   iTimeout, dGainH2)   == false ||
      readSdoObject(iNode, iIndex, SI_GAIN_H3,   iTimeout, dGainH3)   == false ||
      readSdoObject(iNode, iIndex, SI_GAIN_TEMP, iTimeout, dGainTemp) == false)
    return false;

  return true;
}

void kernel::pSettingError(const QString& sInfo)
{
  QString sMess;

  sMess  = "  - ";
  sMess += sInfo;
  log(sMess);
}

void kernel::pSeriousError(const QString& sInfo)
{
  QString sMess;

  sMess  = "  *ERROR*: ";
  sMess += sInfo;
  log(sMess);
}

bool kernel::readSdoObject( const int iNode, const int iIndex, const int iSubIndex,
                            const unsigned int iTimeout, int& iResult)
{
  int     no_of_bytes;
  QString sInfo;

  if (iNode != _pCanNode->nodeId())
    {
      sInfo = QString::asprintf("readSdoObject( node=%d not valid, should be %d )",
                                iNode, _pCanNode->nodeId());
      pSeriousError(sInfo);
      return false;
    }

  if (_pCanNode->sdoReadExpedited(iIndex, iSubIndex, &no_of_bytes,
                                  &iResult, iTimeout) == false)
    {
      sInfo = QString::asprintf("sdoReadExpedited( node=%d, index=x%x, subindex=x%x ) failed",
                                iNode, iIndex, iSubIndex);
      pSeriousError(sInfo);
      return false;
    }
  return true;
}

std::string kernel::convertSerial(const int iSerial)
{
  char        cBuf[BUF_SIZE];
  std::string sSerial;

  char c0 = (char)(iSerial & 0x7f);
  char c1 = (char)((iSerial & 0x7f00) >> 8);
  char c2 = (char)((iSerial & 0x7f0000) >> 16);
  char c3 = (char)((iSerial & 0x7f000000) >> 24);

  sprintf(cBuf, "%c%c%c%c", c0,c1,c2,c3);
  sSerial = cBuf;

  return sSerial;
}
