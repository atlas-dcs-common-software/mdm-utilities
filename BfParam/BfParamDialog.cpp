/*
 *  BfParamDialog.cpp ------
 *  The GUI part of the BfParam application.
 *  July 2015, moved to Linux SLC6
 */

#include <QFile>
#include <QImage>
#include <QLabel>
#include <QMessageBox>
#include <QTextEdit>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QDateTime>

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "BfParamDialog.h"
#include "CanInterface.h"
#include "kernel.h"

// Setting variables and defaults
const   QString gsOrganization( "Nikhef"    );
const   QString gsApplication ( "BfParam"   );
const   QString gsVarIntfType ( "intfType"  );
const   QString gsVarPortIndex( "portIndex" );
const   QString gsVarBaudrate ( "baudrate"  );
const   QString gsDfltIntfType( "KVASER"    );

const   int     giDfltPortIndex = 0;
const   int     giDfltBaudrate  = 125;
const   bool    gbDfltToDbase   = true;
const   bool    gbDfltVerbose   = false;
const   bool    gbDfltLogfile   = false;

bool    gbStopThread;
bool    gbScanAll;

// -----------------------------------------------------------------------------

// Constructor
BfParamDialog::BfParamDialog() : QDialog()
{
  setupUi( this );

  gbStopThread = false;
  gbScanAll    = false;

  Qt::WindowFlags flags = windowFlags();
  flags |= Qt::MSWindowsFixedSizeDialogHint;
  flags |= Qt::WindowMinimizeButtonHint;
  setWindowFlags(flags);

  cbInterface->clear();
  cbInterface->addItem( "KVASER" );
#ifdef WIN32
  cbInterface->addItem( "SYSTEC" );
  cbInterface->addItem( "PEAK"   );
  cbInterface->addItem( "NICAN"  );
#else
  cbInterface->addItem( "Socket" );
#endif // WIN32

  cbBaudrate->clear();
  cbBaudrate->addItem( QString::number(10) );
  cbBaudrate->addItem( QString::number(20) );
  cbBaudrate->addItem( QString::number(50) );
  cbBaudrate->addItem( QString::number(125) ); // default
  cbBaudrate->addItem( QString::number(250) );
  cbBaudrate->addItem( QString::number(500) );
  cbBaudrate->addItem( QString::number(800) );
  cbBaudrate->addItem( QString::number(1000) );

  this->readAppSettings();

  connect(this->butQuit,     SIGNAL(clicked()),                           this, SLOT(onQuit()));
  connect(this->butOpen,     SIGNAL(clicked()),                           this, SLOT(onOpen()));
  connect(this->butScan,     SIGNAL(clicked()),                           this, SLOT(onScan()));
  connect(this->butScanNext, SIGNAL(clicked()),                           this, SLOT(onScanNext()));
  connect(this->butScanAll,  SIGNAL(clicked()),                           this, SLOT(onScanAll()));
  connect(this->cbLogfile,   SIGNAL(clicked()),                           this, SLOT(onLogfile()));
  connect(this->cbToDbase,   SIGNAL(clicked()),                           this, SLOT(onToDbase()));
  connect(this->cbVerbose,   SIGNAL(clicked()),                           this, SLOT(onVerbose()));
  connect(this->cbInterface, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onInterface(const QString&)));

  butScan->setEnabled(false);
  butScanNext->setEnabled(false);
  butScanAll->setEnabled(false);
  cbToDbase->setEnabled(false);
  cbVerbose->setEnabled(false);
  cbLogfile->setEnabled(false);

  _pKernel = new kernel(this);

  connect(_pKernel, SIGNAL(sigLogEdit(const QString&)),     this, SLOT(upd_Stdout(const QString&)));
  connect(_pKernel, SIGNAL(sigOpenCloseStatus(const bool)), this, SLOT(upd_OpenCloseStatus(const bool)));
  connect(_pKernel, SIGNAL(sigWindowTitle(const QString&)), this, SLOT(upd_WindowTitle(const QString&)));
  connect(_pKernel, SIGNAL(sigPortNumber(const int)),       this, SLOT(upd_PortNumber(const int)));
  connect(_pKernel, SIGNAL(sigEndScanAll()),                this, SLOT(upd_EndScanAll()));

  if (_pKernel->initKernel() == false)
    {
      upd_Stdout("FATAL: initKernel() failed");
      return;
    }

  _pKernel->start();

  cbToDbase->setEnabled(true);
  cbToDbase->setChecked(gbDfltToDbase);
  onToDbase();

  cbVerbose->setEnabled(true);
  cbVerbose->setChecked(gbDfltVerbose);
  onVerbose();

  cbLogfile->setEnabled(true);
  cbLogfile->setChecked(gbDfltLogfile);
  onLogfile();

  emit sigPortCount(cbPort->count());
}

// Destructor
BfParamDialog::~BfParamDialog()
{
  this->cleanUp();
}

void BfParamDialog::onQuit()
{
  this->cleanUp();
  this->writeAppSettings();
  exit(0);
}

void BfParamDialog::cleanUp()
{
  gbStopThread = true;
  if (_pKernel->getStatus() == true)
    {
      _pKernel->wait();
      delete _pKernel;
    }
}

void BfParamDialog::onOpen()
{
  QString sInterface = cbInterface->currentText();
  int     iPortNo    = cbPort->currentText().toInt();
  int     iBaudrate  = cbBaudrate->currentText().toInt();

  emit sigOpenClose(sInterface, _dInterfaceType, iPortNo, iBaudrate);
}

void BfParamDialog::onScan()
{
  emit sigScan();
}

void BfParamDialog::onScanNext()
{
  emit sigScanNext();
}

void BfParamDialog::onScanAll()
{
  butQuit->setEnabled(gbScanAll);
  butOpen->setEnabled(gbScanAll);
  butScan->setEnabled(gbScanAll);
  butScanNext->setEnabled(gbScanAll);
  cbToDbase->setEnabled(gbScanAll);
  cbVerbose->setEnabled(gbScanAll);
  cbLogfile->setEnabled(gbScanAll);

  if (gbScanAll == true)
    {
      gbScanAll = false;
      butScanAll->setText("Scan all ports");
      butScanAll->setEnabled(false);
    }
  else
    {
      gbScanAll = true;
      butScanAll->setText("Stop total scan");
      emit sigScanAll();
    }
}

void BfParamDialog::onToDbase()
{
  emit sigToDbase(cbToDbase->isChecked());
}

void BfParamDialog::onVerbose()
{
  emit sigVerbose(cbVerbose->isChecked());
}

void BfParamDialog::onLogfile()
{
  emit sigLogfile(cbLogfile->isChecked());
}

void BfParamDialog::onInterface(const QString& sIntfType)
{
  cbPort->clear();  // clear the port combobox

  _dInterfaceType = -1;
  if (sIntfType == QString("KVASER"))
    _dInterfaceType = KVASER_INTF_TYPE;
  else if (sIntfType == QString("SYSTEC"))
    _dInterfaceType = SYSTEC_INTF_TYPE;
  else if (sIntfType == QString("PEAK"))
    _dInterfaceType = PEAK_INTF_TYPE;
  else if (sIntfType == QString("NICAN"))
    _dInterfaceType = NICAN_INTF_TYPE;
  else if (sIntfType == QString("Socket"))
    _dInterfaceType = SOCKET_INTF_TYPE;

  // determine the number of ports available of the seclected type
  int  no_of_ports  = 0;
  int* port_numbers = 0;

  no_of_ports = CanInterface::canInterfacePorts(_dInterfaceType, &port_numbers);

  if (_dInterfaceType < 0)
    no_of_ports = 0;

  // fill the Port combobox
  for(int i = 0; i < no_of_ports; ++i)
    this->cbPort->addItem( QString::number(port_numbers[i]) );

  bool    bEnabled = (no_of_ports > 0 ? true : false);
  butOpen->setEnabled(bEnabled);
  cbBaudrate->setEnabled(bEnabled);
}

void BfParamDialog::upd_Stdout(const QString& text)
{
  logStdout->append(text);
  logStdout->ensureCursorVisible();
  QCoreApplication::processEvents();
}

void BfParamDialog::upd_OpenCloseStatus(const bool bOpen)
{
  if (bOpen == true)
    butOpen->setText("Open");
  else
    butOpen->setText("Close");

  butScan->setDisabled(bOpen);
  butScanNext->setDisabled(bOpen);
  butScanAll->setDisabled(bOpen);

  cbInterface->setEnabled(bOpen);
  cbPort->setEnabled(bOpen);
  cbBaudrate->setEnabled(bOpen);
}

void BfParamDialog::upd_WindowTitle(const QString& sTitle)
{
  setWindowTitle(sTitle);
}

void BfParamDialog::upd_PortNumber(const int iPort)
{
  cbPort->setCurrentIndex(iPort);
  QCoreApplication::processEvents();
}

void BfParamDialog::upd_EndScanAll()
{
  gbScanAll = false;
  butScanAll->setText("Scan all ports");
  butQuit->setEnabled(true);
  butOpen->setEnabled(true);
  butScan->setEnabled(true);
  butScanNext->setEnabled(true);
  butScanAll->setEnabled(true);
}

void BfParamDialog::readAppSettings()
{
  int        iIndex;
  int        iPort;
  int        iBaudrate;
  QString    sIntfType;
  QString    sBaudrate;
  QSettings  settings( gsOrganization, gsApplication );

  sIntfType = settings.value( gsVarIntfType, gsDfltIntfType ).toString();
  iIndex = cbInterface->findText( sIntfType );
  cbInterface->setCurrentIndex( iIndex );
  // call onInterface in order to fill the Port combobox
  this->onInterface( sIntfType );

  iPort = settings.value( gsVarPortIndex, giDfltPortIndex ).toInt();
  if (iPort >= 0 && iPort < cbPort->count())
    cbPort->setCurrentIndex( iPort );

  iBaudrate = settings.value( gsVarBaudrate, giDfltBaudrate ).toInt();
  sBaudrate = QString::number( iBaudrate );
  iIndex = cbBaudrate->findText( sBaudrate );
  cbBaudrate->setCurrentIndex( iIndex );
}

void BfParamDialog::writeAppSettings()
{
  int       iPort;
  QSettings settings( gsOrganization, gsApplication );

  settings.setValue( gsVarIntfType, cbInterface->currentText());
  iPort = cbPort->currentIndex();
  if (iPort >= 0)
    settings.setValue( gsVarPortIndex, iPort);
  settings.setValue( gsVarBaudrate, cbBaudrate->currentText().toInt() );
}
