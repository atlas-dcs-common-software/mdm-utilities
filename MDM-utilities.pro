#
# Qmake 'pro' file for the CANtools subprojects
#
# For Windows:
# to generate a Visual Studio solution file and project files:
#   qmake -tp vc <name>.pro -r
# or,
# using the Qt Visual Studio Add-In, select 'Open Qt Project File..."
#
# For Linux:
#   qmake <name>.pro -r
#

TEMPLATE = subdirs

# Executables
SUBDIRS += MdtElmb/MdtElmb.pro
SUBDIRS += sCanBus/sCanBus.pro
SUBDIRS += BfParam/BfParam.pro
