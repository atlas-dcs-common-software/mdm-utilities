/*
 *  kernel.cpp ------
 *  The heart of the sCanBus application.
 */

#include <iostream>
#include <cstdio>
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "kernel.h"
#include "sCanBusDialog.h"
#include "CanInterface.h"
#include "CanNode.h"
#include "confDb.h"
#include "configFile.h"
#include "mdm.h"
#include "mdt_obj_dict.h"

#define SCANBUS_INI_FILE    "./scanbus.ini"

#define BUF_SIZE            1024

#define SETTING_SECTION     "Setting"
#define FIRMWARE            "Firmware"
#define LIFEGUARDING        "LifeGuarding"
#define D_FIRMWARE          "MD25.0001"
#define D_LIFEGUARDING      240

#define MAX_BSENSOR         4

#define MODE_UNDEF          -1
#define MODE_COMMON         0
#define MODE_BSENSOR        1
#define MODE_SETTINGS       2

const   QString gsVersion( "v4.1" );

extern  bool    gbStopThread;
extern  bool    gbScanAll;


// -----------------------------------------------------------------------------

// Constructor
kernel::kernel(sCanBusDialog* pDialog)
  : _pCanInterface(0)
{
  _bStatus        = false;
  _bLogfile       = false;
  _pCanBusDialog  = pDialog;
  _pCanInterface  = NULL;
  _pCanNode       = NULL;
  _pConfDb        = NULL;
  _sHostname      = "?";
  _uMode          = MODE_UNDEF;
  _iPortCount     = -1;
  _dInterfaceType = -1;
  _iPortNo        = -1;
  _iBaudrate      = -1;

  connect(_pCanBusDialog,
          SIGNAL(sigOpenClose(const QString&, const int, const int, const int)),
          this,
          SLOT(upd_OpenClose(const QString&, const int, const int, const int)));

  connect(_pCanBusDialog, SIGNAL(sigScan()),               this, SLOT(upd_Scan()));
  connect(_pCanBusDialog, SIGNAL(sigScanNext()),           this, SLOT(upd_ScanNext()));
  connect(_pCanBusDialog, SIGNAL(sigScanAll()),            this, SLOT(upd_ScanAll()));
  connect(_pCanBusDialog, SIGNAL(sigModeCommon()),         this, SLOT(upd_ModeCommon()));
  connect(_pCanBusDialog, SIGNAL(sigModeBsensor()),        this, SLOT(upd_ModeBsensor()));
  connect(_pCanBusDialog, SIGNAL(sigModeSettings()),       this, SLOT(upd_ModeSettings()));
  connect(_pCanBusDialog, SIGNAL(sigLogfile(const bool)),  this, SLOT(upd_Logfile(const bool)));
  connect(_pCanBusDialog, SIGNAL(sigPortCount(const int)), this, SLOT(upd_PortCount(const int)));
}

// Destructor
kernel::~kernel()
{
  this->cleanUp();
}

void kernel::cleanUp()
{
  if (_pConfDb)
    delete _pConfDb;
  if (_pCanNode)
    delete _pCanNode;
  if (_pCanInterface)
    delete _pCanInterface;

  _pCanInterface = NULL;
  _pCanNode      = NULL;
  _pConfDb       = NULL;
}

bool kernel::initKernel()
{
  if (_bStatus == true)
    return true;

#ifdef WIN32
  // Initialize Winsock
  WSADATA wsadata;
  int err = WSAStartup( 0x0202, &wsadata );
  if( err != 0 )
    log( QString::asprintf("ERROR: WSAStartup failed, error code %d",err) );
  else if( wsadata.wVersion != 0x0202 )
    log( QString::asprintf("ERROR: Winsock version: 0x%X", wsadata.wVersion) );
#endif // WIN32

  std::string sIniFile = SCANBUS_INI_FILE;
  configFile* pConfig  = new configFile();
  _sDfltFirmware       = pConfig->getStringVal(sIniFile, FIRMWARE,     D_FIRMWARE);
  _dDfltLifeGuarding   = pConfig->getIntVal   (sIniFile, LIFEGUARDING, D_LIFEGUARDING);

  char host[BUF_SIZE];
  if (gethostname(host, sizeof(host)) == 0)
    _sHostname = host;

  _pConfDb = new confDb(sIniFile);
  _bConfDbStat = _pConfDb->getStatus();
  if (_bConfDbStat == false)
    {
      QString sError;
      sError = "WARNING: confDb constructor failed";
      log(sError);
      sError = _pConfDb->getError().c_str();
      log(sError);
    }
  //_pConfDb->showCanbusList();

  wrIniUsedFile(sIniFile);

  _pMdmArray.clear();

  setWindowTitle();

  QString sInfo;
  sInfo = QString::asprintf("On host: %s", _sHostname.c_str());
  log(sInfo);
  _bStatus = true;
  return _bStatus;
}

void kernel::run()
{
  if (_bStatus == false)
    return;

  while(gbStopThread == false)
    {
      sleep(1);
      qApp->processEvents();
    }
}

void kernel::upd_OpenClose( const QString& sInterfaceType,
                            const int      dInterfaceType,
                            const int      iPortNo,
                            const int      iBaudrate )
{
  _sInterfaceType = sInterfaceType;
  _dInterfaceType = dInterfaceType;
  _iPortNo        = iPortNo;
  _iBaudrate      = iBaudrate;

  openCloseCanPort(true, sInterfaceType, dInterfaceType, iPortNo, iBaudrate, false);
}

void kernel::upd_Scan()
{
  doScan();
}

void kernel::upd_ScanNext()
{
  doScanNext();
}

void kernel::upd_ScanAll()
{
  doScanAll();
}

void kernel::upd_ModeCommon()
{
  _uMode = MODE_COMMON;
}

void kernel::upd_ModeBsensor()
{
  _uMode = MODE_BSENSOR;
}

void kernel::upd_ModeSettings()
{
  _uMode = MODE_SETTINGS;
}

void kernel::upd_Logfile(const bool bLogfile)
{
  _bLogfile = bLogfile;
}

void kernel::upd_PortCount(const int iPortCount)
{
  _iPortCount = iPortCount;
}

void kernel::openCloseCanPort( const bool bReportError,
                               const QString& sInterfaceType,
                               const int dInterfaceType,
                               const int iPortNo,
                               const int iBaudrate,
                               const bool bNext)
{
  QString sError;

  if (_pCanInterface)
    {
      // close/delete the Can Node and Interface port object
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = NULL;
      delete _pCanInterface;
      _pCanInterface = NULL;
      if (bNext == false)
        {
          emit sigOpenCloseStatus(true);
          return;
        }
    }

  _pCanInterface = CanInterface::newCanInterface(dInterfaceType, iPortNo, iBaudrate);

  if (_pCanInterface)
    {
      if (_pCanInterface->opStatus() != 0)
        {
          if (bReportError == true)
            {
              sError  = sInterfaceType;
              sError += " CAN port: \n";
              sError += _pCanInterface->errString().c_str();
              log(sError);
            }
          // Delete the interface object
          delete _pCanInterface;
          _pCanInterface = NULL;
          return;
        }

      // CAN port opened successfully
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = new CanNode(_pCanInterface, 0);

      if (bNext == false)
        {
          emit sigOpenCloseStatus(false);
        }

      QString     sInfo, sTmp;
      std::string sCanbus;

      sInfo  = "\nOpen CAN bus (";
      sInfo += sInterfaceType;
      sTmp = QString::asprintf(", %2d, %d)", iPortNo, iBaudrate);
      sInfo += sTmp;
      sTmp = "";
      if (_pConfDb->getCanbus(_sHostname, iPortNo, sCanbus) == true)
        sTmp = QString::asprintf(": %s", sCanbus.c_str());
      sInfo += sTmp;
      log(sInfo);
      return;
    }

  if (bReportError == true)
    {
      sError  = sInterfaceType;
      sError += " CAN port: \n";
      sError += "Failed to create the CAN port object";
      log(sError);
    }
}

void kernel::log(const QString& sMess)
{
  emit sigLogEdit(sMess);

  if (_bLogfile == false)
    return;

  char   cBuf[BUF_SIZE];
  time_t clock;
  time(&clock);

  struct tm* tInfo = localtime(&clock);

  sprintf(cBuf, "./sblog_%02d%02d%02d",
          tInfo->tm_year-100, tInfo->tm_mon+1, tInfo->tm_mday);
  std::string sFile = cBuf;

  FILE *fp;
  fp = fopen(sFile.c_str(), "a+");
  if (fp == NULL)
    {
      QString sInfo;
      sInfo = QString::asprintf("Cannot write logfile: %s", sFile.c_str());
      _bLogfile = false;    // no recursion
      emit sigLogEdit(sInfo);
      return;
    }

  std::string sUtf8Mess = sMess.toUtf8().constData();
  fprintf(fp, "%s\n", sUtf8Mess.c_str());
  fclose(fp);
}


void kernel::setWindowTitle()
{
  QString sWindowTitle = " sCanBus [" + gsVersion + "]";

  if (_bConfDbStat == true)
    {
      QString sConfDb;
      sConfDb = QString::asprintf(", %s@%s", _pConfDb->getUsername().c_str(),
                                  _pConfDb->getDatabase().c_str());
      sWindowTitle += sConfDb;
    }
  emit sigWindowTitle(sWindowTitle);
}

void kernel::wrIniUsedFile(const std::string& sIniFile)
{
  std::string sIniUsedFile = sIniFile + ".used";

  FILE    *fp;
  fp = fopen(sIniUsedFile.c_str(), "w");
  if (fp == NULL)
    return;

  fprintf(fp, "#\n# INI_FILE =  %s\n#\n", SCANBUS_INI_FILE);
  fprintf(fp, "[%s]\n", SETTING_SECTION);
  fprintf(fp, "%-14s= %s\n", FIRMWARE,     _sDfltFirmware.c_str());
  fprintf(fp, "%-14s= %d\n", LIFEGUARDING, _dDfltLifeGuarding);
  fprintf(fp, "\n");
  fclose(fp);

  if (_bConfDbStat == true)
    {
      if (_pConfDb->wrIniUsedFile(sIniUsedFile) == false)
        {
          QString sInfo;
          sInfo = QString::asprintf("pConfDb: cannot write ini file %s",
                                    sIniUsedFile.c_str());
          log(sInfo);
        }
    }
}

void kernel::doScan()
{
  QString  sInfo;

  _pMdmArray.clear();

  int nNodes = _pCanInterface->scanBus();

  for(int i = 0; i < nNodes; ++i)
    {
      bool        bFound;
      std::string sChamber;

      int         iSerial  = _pCanInterface->scannedSerialNr(i);
      std::string sSerial  = convertSerial(iSerial);
      std::string sVersion = _pCanInterface->scannedSwVersion(i);
      int         iNodeId  = _pCanInterface->scannedNodeId(i);
      mdm*        pMdm     = new mdm(sSerial, iNodeId, sVersion);

      getBsensorItems(iNodeId, pMdm);

      if (_pConfDb->getChamber(sSerial, bFound, sChamber) == false)
        {
          sInfo = QString::asprintf("getChamber(%s) failed [%s]",
                                    sSerial.c_str(), _pConfDb->getError().c_str());
          log(sInfo);
          bFound = false;
        }

      if (bFound == true)
        {
          pMdm->setChamber(bFound, sChamber);

          std::string sDbSerial;
          std::string sDbType;
          int         dDbId, nDbTsens, nDbMezz, dDbBmask, dDbMezzMask;

          if (_pConfDb->getConfig(sChamber, bFound, sDbSerial, sDbType, dDbId,
                                  nDbTsens, nDbMezz, dDbBmask, dDbMezzMask) == false)
            {
              sInfo = QString::asprintf("getConfig(%s) failed [%s]",
                                        sChamber.c_str(), _pConfDb->getError().c_str());
              log(sInfo);
              bFound = false;
            }

          if (bFound == true)
            {
              pMdm->setConfig(bFound, sDbSerial, sDbType, dDbId,
                              nDbTsens, nDbMezz, dDbBmask, dDbMezzMask);
            }

        }
      _pMdmArray.push_back(*pMdm);
      delete pMdm;
    }

  switch(_uMode)
    {
    case MODE_COMMON:
      sortMdmArray();
      doModeCommon();
      break;
    case MODE_BSENSOR:
      sortMdmArray();
      doModeBsensor();
      break;
    case MODE_SETTINGS:
      doModeSetting();
      break;
    default:
      log("Internal error: mode radio button not set");
      return;
    }
}

void kernel::doScanNext()
{
  int iPort = _iPortNo;

  ++iPort;
  if (iPort >= _iPortCount)
    iPort = 0;
  _iPortNo = iPort;

  openCloseCanPort(true, _sInterfaceType, _dInterfaceType, _iPortNo, _iBaudrate, true);

  emit sigPortNumber(_iPortNo);

  doScan();
}

void kernel::doScanAll()
{
  for(int iPort = 0; iPort < _iPortCount; ++iPort)
    {
      if (gbScanAll == false)
        break;
      _iPortNo = iPort;
      openCloseCanPort(true, _sInterfaceType, _dInterfaceType, _iPortNo, _iBaudrate, true);
      emit sigPortNumber(_iPortNo);
      doScan();
      sleep(1);
    }
  emit sigEndScanAll();
}

void kernel::getBsensorItems(const int iNode, mdm* pMdm)
{
  int     dBmask;
  QString sInfo;

  if (_pCanNode->setNodeId(iNode) == false)
    {
      sInfo = QString::asprintf("setNodeId( %3d ) failed", iNode);
      pSeriousError(sInfo);
      return;
    }

  if (readSdoObject(iNode, OD_BF_NSENSORS, 0, 200, dBmask) == false)
    {
      dBmask = -1;
    }
  pMdm->_dBmask = dBmask;

  if (dBmask > 0)
    {
      if ((dBmask & 0x1))
        getBsensorId(iNode, 0, pMdm->_sBsens0);
      if ((dBmask & 0x2))
        getBsensorId(iNode, 1, pMdm->_sBsens1);
      if ((dBmask & 0x4))
        getBsensorId(iNode, 2, pMdm->_sBsens2);
      if ((dBmask & 0x8))
        getBsensorId(iNode, 3, pMdm->_sBsens3);
    }
}

void kernel::getBsensorId(const int iNode, const int dSensor, std::string& sId)
{
  int iIndex;
  int iSubIndex;

  sId = "?";

  switch(dSensor)
    {
    case 0: iIndex = OD_BF_ID0; break;
    case 1: iIndex = OD_BF_ID1; break;
    case 2: iIndex = OD_BF_ID2; break;
    case 3: iIndex = OD_BF_ID3; break;
    default: 
      QString sInfo;
      sInfo = QString::asprintf("getBsensorId( node=%d, sensor=%d ) illegal",
                                iNode, dSensor);
      pSeriousError(sInfo);
      return;
    }

  unsigned int iTimeout = 400;
  int          iIdLow, iIdHigh;

  iSubIndex = 1;
  if (readSdoObject(iNode, iIndex, iSubIndex, iTimeout, iIdLow) == false)
    return;

  iSubIndex = 2;
  if (readSdoObject(iNode, iIndex, iSubIndex, iTimeout, iIdHigh) == false)
    return;

  char    cBuf[BUF_SIZE];
  sprintf(cBuf, "x%08X%08X", iIdHigh, iIdLow);
  sId = cBuf;
}

bool kernel::getLifeGuarding(const int iNode, int& dLifeGuarding)
{
  return readSdoObject(iNode, OD_LIFE_GUARDING, 0, 200, dLifeGuarding);
}

bool kernel::getPdoEnabled(const int iNode, const int dPdo, bool& bEnabled)
{
  int     iIndex = 0;
  int     iResult;
  QString sInfo;

  switch(dPdo)
    {
    case 1: iIndex = OD_TXPDO1; break;
    case 2: iIndex = OD_TXPDO2; break;
    case 3: iIndex = OD_TXPDO3; break;
    case 4: iIndex = OD_TXPDO4; break;
    default:
      sInfo = QString::asprintf("getPdoEnabled( node=%d, pdo=%d ) illegal",
                                _pCanNode->nodeId(), dPdo);
      pSeriousError(sInfo);
      return false;
    }

  if (readSdoObject(iNode, iIndex, 2, 200, iResult) == false)
    return false;
  bEnabled = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getDigInitOutput(const int iNode, int& dDigInitOutput)
{
  return readSdoObject(iNode, OD_INIT_DIG_OUTPUT, 0, 200, dDigInitOutput);
}

bool kernel::getDigIoEnabled(const int iNode, bool& bDigIoEnabled)
{
  int iResult;

  if (readSdoObject(iNode, OD_DIGIO_ENABLED, 0, 200, iResult) == false)
    return false;
  bDigIoEnabled = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getElmbAdcEnabled(const int iNode, bool& bElmbAdcEnabled)
{
  int iResult;

  if (readSdoObject(iNode, OD_ELMB_ADC_ENABLED, 0, 200, iResult) == false)
    return false;
  bElmbAdcEnabled = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getNtcConv(const int iNode, int& dConv)
{
  return readSdoObject(iNode, OD_ELMB_ADC_NTCCONV, 0, 200, dConv);
}

bool kernel::getAdcRawData(const int iNode, bool& bRawData)
{
  int iResult;

  if (readSdoObject(iNode, OD_ELMB_ADC_RAWDATA, 0, 200, iResult) == false)
    return false;
  bRawData = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getElmbAdcParams(const int iNode, int& dNchan, int& dRate,
                              int& dRange, int& dMode)
{
  int iIndex = OD_ELMB_ADC;
  unsigned int iTimeout = 200;

  if (readSdoObject(iNode, iIndex, SI_CHANNELS,  iTimeout, dNchan) == false ||
      readSdoObject(iNode, iIndex, SI_RATE,      iTimeout, dRate)  == false ||
      readSdoObject(iNode, iIndex, SI_RANGE,     iTimeout, dRange) == false ||
      readSdoObject(iNode, iIndex, SI_MODE,      iTimeout, dMode)  == false)
    return false;
  return true;
}

bool kernel::getCsmAdcEnabled(const int iNode, bool& bCsmAdcEnabled)
{
  int iResult;

  if (readSdoObject(iNode, OD_CSM_ADC_ENABLED, 0, 200, iResult) == false)
    return false;
  bCsmAdcEnabled = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getCsmAdcCalibrate(const int iNode, bool& bCalibrate)
{
  int iResult;

  if (readSdoObject(iNode, OD_CSM_ADC_CALIBRATE, 0, 200, iResult) == false)
    return false;
  bCalibrate = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getCsmAdcParams(const int iNode, int& dNchan, int& dRate,
                             int& dRange, int& dMode, int& dRefChan)
{
  int iIndex   = OD_CSM_ADC;
  unsigned int iTimeout = 200;

  if (readSdoObject(iNode, iIndex, SI_CHANNELS,  iTimeout, dNchan)   == false ||
      readSdoObject(iNode, iIndex, SI_RATE,      iTimeout, dRate)    == false ||
      readSdoObject(iNode, iIndex, SI_RANGE,     iTimeout, dRange)   == false ||
      readSdoObject(iNode, iIndex, SI_MODE,      iTimeout, dMode)    == false ||
      readSdoObject(iNode, iIndex, SI_REFCHAN,   iTimeout, dRefChan) == false)
    return false;
  return true;
}

bool kernel::getCsmMezzMask(const int iNode, int& dCurMezzMask)
{
  return readSdoObject(iNode, OD_CSM_ADC, SI_MEZZMASK, 200, dCurMezzMask);
}

bool kernel::getBfAdcCalibrate(const int iNode, bool& bCalibrate)
{
  int iResult;

  if (readSdoObject(iNode, OD_BF_ADC_CALIBRATE, 0, 200, iResult) == false)
    return false;
  bCalibrate = (iResult == 1 ? true : false);
  return true;
}

bool kernel::getBfAdcParams(const int iNode, const int dSensor,
                            int& dHallRate, int& dHallRange, int& dHallMode,
                            int& dTempRate, int& dTempRange, int& dTempMode)
{
  int iIndex = 0;
  unsigned int iTimeout = 200;

  switch(dSensor)
    {
    case 0: iIndex = OD_BF_ADC_B0; break;
    case 1: iIndex = OD_BF_ADC_B1; break;
    case 2: iIndex = OD_BF_ADC_B2; break;
    case 3: iIndex = OD_BF_ADC_B3; break;
    default:
      QString sInfo;
      sInfo = QString::asprintf("getBfAdcParams( node=%d, sensor=%d ) illegal",
                                iNode, dSensor);
      pSeriousError(sInfo);
      return false;
    }

  if (readSdoObject(iNode, iIndex, SI_HALL_RATE,  iTimeout, dHallRate)  == false ||
      readSdoObject(iNode, iIndex, SI_HALL_RANGE, iTimeout, dHallRange) == false ||
      readSdoObject(iNode, iIndex, SI_HALL_MODE,  iTimeout, dHallMode)  == false ||
      readSdoObject(iNode, iIndex, SI_TEMP_RATE,  iTimeout, dTempRate)  == false ||
      readSdoObject(iNode, iIndex, SI_TEMP_RANGE, iTimeout, dTempRange) == false ||
      readSdoObject(iNode, iIndex, SI_TEMP_MODE,  iTimeout, dTempMode)  == false)
    return false;
  return true;
}

void kernel::sortMdmArray()
{
  int         iTmp;
  bool        bTmp;
  std::string sTmp;

  size_t nNodes = _pMdmArray.size();

  for(size_t i = 0; i < nNodes-1; ++i)
    {
      for(size_t j = i+1; j < nNodes; ++j)
        {
          mdm* pMdm1 = &_pMdmArray[i];
          mdm* pMdm2 = &_pMdmArray[j];

          if (pMdm1->_sChamber > pMdm2->_sChamber)
            {
              sTmp = pMdm1->_sChamber;  pMdm1->_sChamber  = pMdm2->_sChamber;  pMdm2->_sChamber  = sTmp;
              sTmp = pMdm1->_sSerial;   pMdm1->_sSerial   = pMdm2->_sSerial;   pMdm2->_sSerial   = sTmp;
              sTmp = pMdm1->_sVersion;  pMdm1->_sVersion  = pMdm2->_sVersion;  pMdm2->_sVersion  = sTmp;
              sTmp = pMdm1->_sDbSerial; pMdm1->_sDbSerial = pMdm2->_sDbSerial; pMdm2->_sDbSerial = sTmp;
              sTmp = pMdm1->_sDbType;   pMdm1->_sDbType   = pMdm2->_sDbType;   pMdm2->_sDbType   = sTmp;
              sTmp = pMdm1->_sBsens0;   pMdm1->_sBsens0   = pMdm2->_sBsens0;   pMdm2->_sBsens0   = sTmp;
              sTmp = pMdm1->_sBsens1;   pMdm1->_sBsens1   = pMdm2->_sBsens1;   pMdm2->_sBsens1   = sTmp;
              sTmp = pMdm1->_sBsens2;   pMdm1->_sBsens2   = pMdm2->_sBsens2;   pMdm2->_sBsens2   = sTmp;
              sTmp = pMdm1->_sBsens3;   pMdm1->_sBsens3   = pMdm2->_sBsens3;   pMdm2->_sBsens3   = sTmp;

              bTmp = pMdm1->_bChamberFound;
              pMdm1->_bChamberFound = pMdm2->_bChamberFound;
              pMdm2->_bChamberFound = bTmp;

              bTmp = pMdm1->_bConfigFound;
              pMdm1->_bConfigFound  = pMdm2->_bConfigFound;
              pMdm2->_bConfigFound  = bTmp;

              iTmp = pMdm1->_dId;         pMdm1->_dId         = pMdm2->_dId;         pMdm2->_dId         = iTmp;
              iTmp = pMdm1->_dBmask;      pMdm1->_dBmask      = pMdm2->_dBmask;      pMdm2->_dBmask      = iTmp;
              iTmp = pMdm1->_dDbId;       pMdm1->_dDbId       = pMdm2->_dDbId;       pMdm2->_dDbId       = iTmp;
              iTmp = pMdm1->_nDbTsens;    pMdm1->_nDbTsens    = pMdm2->_nDbTsens;    pMdm2->_nDbTsens    = iTmp;
              iTmp = pMdm1->_nDbMezz;     pMdm1->_nDbMezz     = pMdm2->_nDbMezz;     pMdm2->_nDbMezz     = iTmp;
              iTmp = pMdm1->_dDbBmask;    pMdm1->_dDbBmask    = pMdm2->_dDbBmask;    pMdm2->_dDbBmask    = iTmp;
              iTmp = pMdm1->_dDbMezzMask; pMdm1->_dDbMezzMask = pMdm2->_dDbMezzMask; pMdm2->_dDbMezzMask = iTmp;

            }
        }
    }
}

void kernel::doModeCommon()
{
  QString sInfo, sResult;
  size_t  nNodes = _pMdmArray.size();

  if (nNodes > 0)
    {
      log("\nchamber   id  ser.   version              confdb");
      log("-----------------------------------------------------------");
      for(size_t i = 0; i < nNodes; ++i)
        {
          mdm* pMdm = &_pMdmArray[i];
          bool bChamberFound = pMdm->_bChamberFound;
          bool bConfigFound  = pMdm->_bConfigFound;

          sResult = QString::asprintf("%-7s  %3d  %-4s  %-9s  ",
                                      pMdm->_sChamber.c_str(), pMdm->_dId,
                                      pMdm->_sSerial.c_str(), pMdm->_sVersion.c_str());

          if (bChamberFound == false)
            sResult += "              -";
          else if (bConfigFound == false)
            sResult += "        [ not found ]";
          else
            {
              sInfo = QString::asprintf("[%s,%s,%3d,%2d,%2d,%2d,x%05X]",
                                        pMdm->_sDbSerial.c_str(), pMdm->_sDbType.c_str(),
                                        pMdm->_dDbId, pMdm->_nDbTsens, pMdm->_nDbMezz,
                                        pMdm->_dDbBmask, pMdm->_dDbMezzMask);
              sResult += sInfo;
              if (pMdm->_dId != pMdm->_dDbId)
                sResult += "  [ID]";
              if (pMdm->_sSerial != pMdm->_sDbSerial)
                sResult += "  [SERIAL]";
            }
          log(sResult);
        }
    }

  sInfo = QString::asprintf("\n#nodes: %lu", nNodes);
  log(sInfo);
}

void kernel::doModeBsensor()
{
  QString sInfo, sResult;
  size_t  nNodes = _pMdmArray.size();

  if (nNodes > 0)
    {
      log("\nchamber   id  ser. mask conf          S0                S1                S2                S3");
      log("-----------------------------------------------------------------------------------------------------");
      for(size_t i = 0; i < nNodes; ++i)
        {
          mdm*    pMdm          = &_pMdmArray[i];
          int     dBmask        = pMdm->_dBmask;
          bool    bChamberFound = pMdm->_bChamberFound;

          sResult = QString::asprintf("%-7s  %3d  %-4s  ",
                                      pMdm->_sChamber.c_str(), pMdm->_dId,
                                      pMdm->_sSerial.c_str());
          if (dBmask < 0)
            sResult += " ?";
          else
            {
              sInfo = QString::asprintf("x%X", dBmask);
              sResult += sInfo;
            }
          sResult += "   ";

          if (bChamberFound == false)
            sResult += " -";
          else
            {
              if (pMdm->_dDbBmask < 0)
                sResult += " ?";
              else
                {
                  sInfo = QString::asprintf("x%X", pMdm->_dDbBmask);
                  sResult += sInfo;
                }
            }
          sInfo = QString::asprintf("   %-18s%-18s%-18s%-18s",
                                    pMdm->_sBsens0.c_str(), pMdm->_sBsens1.c_str(),
                                    pMdm->_sBsens2.c_str(), pMdm->_sBsens3.c_str());
          sResult += sInfo;
          log(sResult);
        }
    }

  sInfo = QString::asprintf("\n#nodes: %lu", nNodes);
  log(sInfo);

}

void kernel::doModeSetting()
{
  QString sInfo;
  size_t  nNodes = _pMdmArray.size();

  if (nNodes > 0)
    {
      for(size_t i = 0; i < nNodes; ++i)
        {
          mdm*    pMdm = &_pMdmArray[i];
          handleNodeSettings(pMdm);
        }
    }

  sInfo = QString::asprintf("\n#nodes: %lu", nNodes);
  log(sInfo);
}

void kernel::handleNodeSettings(mdm* pMdm)
{
  QString sInfo;
  int     iNode = pMdm->_dId;

  if (_bConfDbStat == false)
    {
      sInfo = QString::asprintf("NODE %3d: SERIAL %s",
                                iNode, pMdm->_sSerial.c_str());
      log(sInfo);
      return;
    }

  if (pMdm->_bChamberFound == false)
    {
      sInfo = QString::asprintf("NODE %3d: SERIAL %s: not in ConfDb",
                                iNode, pMdm->_sSerial.c_str());
      log(sInfo);
      return;
    }
  if (pMdm->_bConfigFound == false)
    {
      sInfo = QString::asprintf("%-7s: NODE %3d: SERIAL %s: no data in ConfDb",
                                pMdm->_sChamber.c_str(), iNode, pMdm->_sSerial.c_str());
      log(sInfo);
      return;
    }

  QString sMain;
  sMain = QString::asprintf("%-7s: NODE %3d: SERIAL %s: CONFDB: [%s,%s,%3d,%2d,%2d,%2d,x%05X]",
                            pMdm->_sChamber.c_str(), iNode, pMdm->_sSerial.c_str(),
                            pMdm->_sDbSerial.c_str(), pMdm->_sDbType.c_str(), pMdm->_dDbId, pMdm->_nDbTsens,
                            pMdm->_nDbMezz, pMdm->_dDbBmask, pMdm->_dDbMezzMask);
  log(sMain);

  if (iNode != pMdm->_dDbId)
    {
      sInfo = QString::asprintf("ConfDb: wrong ID %d [%d]",
                                pMdm->_dDbId, iNode);
      pSettingError(sInfo);
    }
  if (pMdm->_sSerial != pMdm->_sDbSerial)
    {
      sInfo = QString::asprintf("ConfDb: wrong serial %s [%s]",
                                pMdm->_sDbSerial.c_str(), pMdm->_sSerial.c_str());
      pSettingError(sInfo);
    }
  if (pMdm->_sVersion != _sDfltFirmware)
    {
      sInfo = QString::asprintf("Firmware: wrong version %s [%s]",
                                pMdm->_sVersion.c_str(), _sDfltFirmware.c_str());
      pSettingError(sInfo);
    }

  if (_pCanNode->setNodeId(iNode) == false)
    {
      sInfo = QString::asprintf(" *ERROR*: setNodeId( %3d ) failed", iNode);
      log(sInfo);
      return;
    }

  int dLifeGuarding;
  if (getLifeGuarding(iNode, dLifeGuarding) == true)
    {
      if (dLifeGuarding != _dDfltLifeGuarding)
        {
          sInfo = QString::asprintf("LifeGuarding: wrong value %d [%d]",
                                    dLifeGuarding, _dDfltLifeGuarding);
          pSettingError(sInfo);
        }
    }

  bool    bPdo1Enabled, bPdo2Enabled, bPdo3Enabled, bPdo4Enabled;
  if (getPdoEnabled(iNode, 1, bPdo1Enabled) == false ||
      getPdoEnabled(iNode, 2, bPdo2Enabled) == false ||
      getPdoEnabled(iNode, 3, bPdo3Enabled) == false ||
      getPdoEnabled(iNode, 4, bPdo4Enabled) == false)
    return;

  checkPdo1Params(pMdm, iNode, bPdo1Enabled);    // DIG-IO
  checkPdo2Params(pMdm, iNode, bPdo2Enabled);    // ELMB-ADC
  checkPdo3Params(pMdm, iNode, bPdo3Enabled);    // CSM-ADC
  checkPdo4Params(pMdm, iNode, bPdo4Enabled);    // BFIELD
}

/*
 * checkPdo1Params ------
 * Check the DIG-IO related parameters (PDO1)
 */
void kernel::checkPdo1Params(mdm* pMdm, const int iNode, bool bPdoEnabled)
{
  QString sInfo;

  if (pMdm->_nDbMezz == 0 && pMdm->_dDbMezzMask == 0)
    return;

  if (bPdoEnabled == false)
    pSettingError("DIG-IO: Enable TXPDO1");

  int     dInitOutput;
  bool    bIoEnabled;
  if (getDigInitOutput(iNode, dInitOutput) == false ||
      getDigIoEnabled(iNode, bIoEnabled) == false)
    return;

  if (dInitOutput != D_INIT_DIG_OUTPUT)
    {
      sInfo = QString::asprintf("DIG-IO: wrong init output x%X [x%X]",
                                dInitOutput, D_INIT_DIG_OUTPUT);
      pSettingError(sInfo);
    }
  if (bIoEnabled == false)
    pSettingError("DIG-IO: Enable change-of-state");
}

/*
 * checkPdo2Params ------
 * Check the ELMB-ADC related parameters (PDO2)
 */
void kernel::checkPdo2Params(mdm* pMdm, const int iNode, bool bPdoEnabled)
{
  QString sInfo;
  bool    bElmbAdcEnabled;

  if (getElmbAdcEnabled(iNode, bElmbAdcEnabled) == false)
    return;

  if (pMdm->_nDbTsens == 0)
    {
      if (bElmbAdcEnabled == true)
        pSettingError("ELMB-ADC: Disable ADC");
      return;
    }

  if (bPdoEnabled == false)
    pSettingError("ELMB-ADC: Enable TXPDO2");
  if (bElmbAdcEnabled == false)
    pSettingError("ELMB-ADC: Enable ADC");

  int     dConv;
  bool    bRawData;
  if (getNtcConv(iNode, dConv)       == false ||
      getAdcRawData(iNode, bRawData) == false)
    return;

  if (dConv != NTC_MDEGREES)
    pSettingError("ELMB-ADC: Switch to millidegrees readout");
  if (pMdm->_sDbType == "E" || pMdm->_sDbType == "e")
    {
      if (bRawData == false)
        pSettingError("ELMB-ADC: Enable RawData");
    }
  else
    {
      if (bRawData == true)
        pSettingError("ELMB-ADC: Disable RawData");
    }

  int dNchan, dRate, dRange, dMode;
  if (getElmbAdcParams(iNode, dNchan, dRate, dRange, dMode) == false)
    return;

  int nChan = pMdm->_nDbTsens * 2;
  if (nChan != dNchan)
    {
      sInfo = QString::asprintf("ELMB-ADC: wrong #channels %d [%d]",
                                dNchan, nChan);
      pSettingError(sInfo);
    }
  if (dRate != D_ELMB_ADC_RATE)
    {
      sInfo = QString::asprintf("ELMB-ADC: wrong rate %d [%d]",
                                dRate, D_ELMB_ADC_RATE);
      pSettingError(sInfo);
    }
  if (dRange != D_ELMB_ADC_RANGE)
    {
      sInfo = QString::asprintf("ELMB-ADC: wrong range %d [%d]",
                                dRange, D_ELMB_ADC_RANGE);
      pSettingError(sInfo);
    }
  if (dMode != D_ELMB_ADC_MODE)
    {
      sInfo = QString::asprintf("ELMB-ADC: wrong mode %d [%d]",
                                dMode, D_ELMB_ADC_MODE);
      pSettingError(sInfo);
    }
}

/*
 * checkPdo3Params ------
 * Check the CSM-ADC related parameters (PDO3)
 */
void kernel::checkPdo3Params(mdm* pMdm, const int iNode, bool bPdoEnabled)
{
  QString sInfo;

  bool    bCsmAdcEnabled;
  if (getCsmAdcEnabled(iNode, bCsmAdcEnabled) == false)
    return;

  if (pMdm->_nDbMezz == 0 && pMdm->_dDbMezzMask == 0)
    {
      if (bCsmAdcEnabled == true)
        pSettingError("CSM-ADC: Disable ADC");
      return;
    }

  if (bPdoEnabled == false)
    pSettingError("CSM-ADC: Enable TXPDO3");
  if (bCsmAdcEnabled == false)
    pSettingError("CSM-ADC: Enable ADC");

  bool    bCalibrate;
  if (getCsmAdcCalibrate(iNode, bCalibrate) == false)
    return;
  if (bCalibrate == false)
    pSettingError("CSM-ADC: Enable Reset/Calibrate");

  int dNchan, dRate, dRange, dMode, dRefChan;
  if (getCsmAdcParams(iNode, dNchan, dRate, dRange, dMode, dRefChan) == false)
    return;

  if (dNchan != D_CSM_ADC_CHANNELS)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong #channels %d [%d]",
                                dNchan, D_CSM_ADC_CHANNELS);
      pSettingError(sInfo);
    }
  if (dRate != D_CSM_ADC_RATE)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong rate %d [%d]",
                                dRate, D_CSM_ADC_RATE);
      pSettingError(sInfo);
    }
  if (dRange != D_CSM_ADC_RANGE)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong range %d [%d]",
                                dRange, D_CSM_ADC_RANGE);
      pSettingError(sInfo);
    }
  if (dMode != D_CSM_ADC_MODE)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong mode %d [%d]",
                                dMode, D_CSM_ADC_MODE);
      pSettingError(sInfo);
    }
  if (dRefChan != D_CSM_ADC_REFCHAN)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong reference channel %d [%d]",
                                dRefChan, D_CSM_ADC_REFCHAN);
      pSettingError(sInfo);
    }

  int dCurMezzMask;
  if (getCsmMezzMask(iNode, dCurMezzMask) == false)
    return;
  if (dCurMezzMask != pMdm->_dDbMezzMask)
    {
      sInfo = QString::asprintf("CSM-ADC: wrong mezz mask x%X [x%X]",
                                dCurMezzMask, pMdm->_dDbMezzMask);
      pSettingError(sInfo);
    }
}

/*
 * checkPdo4Params ------
 * Check the BFIELD related parameters (PDO4)
 */
void kernel::checkPdo4Params(mdm* pMdm, const int iNode, bool bPdoEnabled)
{
  QString sInfo;

  if (pMdm->_dBmask != pMdm->_dDbBmask)
    {
      sInfo = QString::asprintf("B-FIELD: wrong mask x%x [x%X]",
                                pMdm->_dBmask, pMdm->_dDbBmask);
      pSettingError(sInfo);
    }

  if (pMdm->_dDbBmask == 0)
    return;

  if (bPdoEnabled == false)
    pSettingError("B-FIELD: Enable TXPDO4");

  bool    bCalibrate;
  if (getBfAdcCalibrate(iNode, bCalibrate) == false)
    return;
  if (bCalibrate == true)
    pSettingError("B-FIELD: Disable Reset/Calibrate");

  int dSensor;
  for(dSensor = 0; dSensor < MAX_BSENSOR; ++dSensor)
    {
      int dMask = (1 << dSensor);
      if ((pMdm->_dDbBmask & dMask) == 0)
        continue;

      int dHallRate, dHallRange, dHallMode;
      int dTempRate, dTempRange, dTempMode;

      if (getBfAdcParams(iNode, dSensor, dHallRate, dHallRange, dHallMode,
                         dTempRate, dTempRange, dTempMode) == false)
        return;

      if (dHallRate != D_BF_ADC_HALL_RATE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong hall-rate %d [%d]",
                                    dSensor, dHallRate, D_BF_ADC_HALL_RATE);
          pSettingError(sInfo);
        }
      if (dHallRange != D_BF_ADC_HALL_RANGE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong hall-range %d [%d]",
                                    dSensor, dHallRange, D_BF_ADC_HALL_RANGE);
          pSettingError(sInfo);
        }
      if (dHallMode != D_BF_ADC_HALL_MODE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong hall-mode %d [%d]",
                                    dSensor, dHallMode, D_BF_ADC_HALL_MODE);
          pSettingError(sInfo);
        }
      if (dTempRate != D_BF_ADC_TEMP_RATE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong temp-rate %d [%d]",
                                    dSensor, dTempRate, D_BF_ADC_TEMP_RATE);
          pSettingError(sInfo);
        }
      if (dTempRange != D_BF_ADC_TEMP_RANGE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong temp-range %d [%d]",
                                    dSensor, dTempRange, D_BF_ADC_TEMP_RANGE);
          pSettingError(sInfo);
        }
      if (dTempMode != D_BF_ADC_TEMP_MODE)
        {
          sInfo = QString::asprintf("B-SENSOR #%d: wrong temp-mode %d [%d]",
                                    dSensor, dTempMode, D_BF_ADC_TEMP_MODE);
          pSettingError(sInfo);
        }
    }
}

void kernel::pSettingError(const QString& sInfo)
{
  QString sMess;

  sMess  = "  - ";
  sMess += sInfo;
  log(sMess);
}

void kernel::pSeriousError(const QString& sInfo)
{
  QString sMess;

  sMess  = "  *ERROR*: ";
  sMess += sInfo;
  log(sMess);
}

bool kernel::readSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                           const unsigned int iTimeout, int& iResult)
{
  int     no_of_bytes;
  QString sInfo;

  if (iNode != _pCanNode->nodeId())
    {
      sInfo = QString::asprintf("readSdoObject( node=%d not valid, should be %d )",
                                iNode, _pCanNode->nodeId());
      pSeriousError(sInfo);
      return false;
    }

  if (_pCanNode->sdoReadExpedited(iIndex, iSubIndex, &no_of_bytes,
                                  &iResult, iTimeout) == false)
    {
      sInfo = QString::asprintf("sdoReadExpedited( node=%d, index=x%x, subindex=x%x ) failed",
                                iNode, iIndex, iSubIndex);
      pSeriousError(sInfo);
      return false;
    }
  return true;
}

std::string kernel::convertSerial(const int iSerial)
{
  char        cBuf[BUF_SIZE];
  std::string sSerial;

  char c0 = (char)(iSerial & 0x7f);
  char c1 = (char)((iSerial & 0x7f00) >> 8);
  char c2 = (char)((iSerial & 0x7f0000) >> 16);
  char c3 = (char)((iSerial & 0x7f000000) >> 24);

  sprintf(cBuf, "%c%c%c%c", c0,c1,c2,c3);
  sSerial = cBuf;

  return sSerial;
}
