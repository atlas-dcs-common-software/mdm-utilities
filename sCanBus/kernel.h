/*
 *  kernel.h ------
 *  
 */

#ifndef KERNEL_H
#define KERNEL_H

#include <QThread>
#include <QMessageBox>

#define MODE_COMMON     0
#define MODE_BSENSOR    1
#define MODE_SETTINGS   2

// forward declarations
class sCanBusDialog;
class CanInterface;
class CanNode;
class confDb;
class mdm;

class kernel: public QThread
{
    Q_OBJECT

public:
                        kernel(sCanBusDialog* pDialog);
                       ~kernel();

    void                run();
    bool                initKernel();
    bool                getStatus()  { return _bStatus; }

private:
    bool                _bStatus;
    sCanBusDialog*      _pCanBusDialog;
    CanInterface*       _pCanInterface;
    CanNode*            _pCanNode;
    confDb*             _pConfDb;
    bool                _bConfDbStat;
    unsigned int        _uMode;
    std::vector<mdm>    _pMdmArray;
    std::string         _sDfltFirmware;
    int                 _dDfltLifeGuarding;
    bool                _bLogfile;
    QString             _sInterfaceType;
    int                 _dInterfaceType;
    std::string         _sHostname;
    int                 _iPortCount;
    int                 _iPortNo;
    int                 _iBaudrate;

    void                cleanUp();
    void                log(const QString& sMess);
    void                setWindowTitle();
    void                wrIniUsedFile(const std::string& sIniFile);

    void                openCloseCanPort(const bool bReportError, const QString& sInterfaceType,
                                         const int dInterfaceType, const int iPortNo, const int iBaudrate,
                                         const bool bNext);
    void                doScan();
    void                doScanNext();
    void                doScanAll();
    void                doModeCommon();
    void                doModeBsensor();
    void                doModeSetting();
    void                sortMdmArray();
    void                handleNodeSettings(mdm* pMdm);
    void                checkPdo1Params(mdm* pMdm, const int iNode, const bool bPdoEnabled);
    void                checkPdo2Params(mdm* pMdm, const int iNode, const bool bPdoEnabled);
    void                checkPdo3Params(mdm* pMdm, const int iNode, const bool bPdoEnabled);
    void                checkPdo4Params(mdm* pMdm, const int iNode, const bool bPdoEnabled);
    void                pSettingError(const QString& sInfo);
    void                pSeriousError(const QString& sInfo);
    std::string         convertSerial(const int iSerial);
    void                getBsensorItems(const int iNode, mdm* pMdm);
    void                getBsensorId(const int iNode, const int dSensor, std::string& sId);
    bool                getLifeGuarding(const int iNode, int& dLifeGuarding);
    bool                getPdoEnabled(const int iNode, const int dPdo, bool& bPdoEnabled);
    bool                getDigInitOutput(const int iNode, int& dInitOutput);
    bool                getDigIoEnabled(const int iNode, bool& bIoEnabled);
    bool                getElmbAdcEnabled(const int iNode, bool& bElmbAdcEnabled);
    bool                getNtcConv(const int iNode, int& dConv);
    bool                getAdcRawData(const int iNode, bool& bRawData);
    bool                getElmbAdcParams(const int iNode, int& dNchan, int& dRate, int& dRange, int& dMode);
    bool                getCsmAdcEnabled(const int iNode, bool& bCsmAdcEnabled);
    bool                getCsmAdcCalibrate(const int iNode, bool& bCalibrate);
    bool                getCsmAdcParams(const int iNode, int& dNchan, int& dRate, int& dRange, int& dMode, int& dRefChan);
    bool                getCsmMezzMask(const int iNode, int& dCurMezzMask);
    bool                getBfAdcCalibrate(const int iNode, bool& bCalibrate);
    bool                getBfAdcParams(const int iNode, const int dSensor, int& dHallRate, int& dHallRange, int& dHallMode,
                                       int& dTempRate, int& dTempRange, int& dTempMode);
    bool                readSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                                      const unsigned int iTimeout, int &dResult);

private slots:

public  slots:
    void                upd_OpenClose(const QString& sInterfaceType, const int dInterfaceType, const int iPortNo, const int iBaudrate);
    void                upd_Scan();
    void                upd_ScanNext();
    void                upd_ScanAll();
    void                upd_ModeCommon();
    void                upd_ModeBsensor();
    void                upd_ModeSettings();
    void                upd_Logfile(const bool bLogfile);
    void                upd_PortCount(const int iPortCount);

signals:
    void                sigLogEdit(const QString& sMess);
    void                sigOpenCloseStatus(const bool bOpen);
    void                sigWindowTitle(const QString& sTitle);
    void                sigPortNumber(const int iPort);
    void                sigEndScanAll();
};

#endif
