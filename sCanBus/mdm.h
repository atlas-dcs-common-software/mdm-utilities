/*
 * mdm.h ------
 * Container of MDM properties
 */

#ifndef	MDM_H
#define	MDM_H

#include <string>

class mdm
{
public:
		mdm(const std::string sSerial, const int dId, const std::string sVersion);
	       ~mdm();

	void	setChamber(const bool bChamberFound, const std::string sChamber);
	void	setBfield( const int dBmask,
			   const std::string sBsens0, const std::string sBsens1,
			   const std::string sBsens2, const std::string sBsens3);
	void	setConfig( const bool bConfigFound,  const std::string sDbSerial, const std::string sDbType,
			   const int dDbId, const int nDbTsens, const int nDbMezz, const int dDbBmask, const int dDbMezzMask);
	
	std::string	_sSerial;
	int		_dId;
	std::string	_sVersion;
	bool		_bChamberFound;
	std::string	_sChamber;
	int		_dBmask;
	std::string	_sBsens0;
	std::string	_sBsens1;
	std::string	_sBsens2;
	std::string	_sBsens3;
	bool		_bConfigFound;
	std::string	_sDbSerial;
	std::string	_sDbType;
	int		_dDbId;
	int		_nDbTsens;
	int		_nDbMezz;
	int		_dDbBmask;
	int		_dDbMezzMask;
};

#endif
