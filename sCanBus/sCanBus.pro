#
# Project file for the sCanBus application
#
# To generate a Visual Studio project:
#   qmake -t vcapp sCanBus.pro
# To generate a Makefile:
#   qmake sCanBus.pro
#
TEMPLATE = app
TARGET   = sCanBus

# Create a Qt app
CONFIG += qt thread warn_on exceptions debug_and_release
contains(QT_MAJOR_VERSION,5) {
  QT += widgets concurrent
}

CANOPEN_DIR = ../../CANtools/libs/CANopen

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  LIBS       += -L../../CANtools/Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  LIBS       += -L../../CANtools/Release
}

LIBS += -lCANopen

unix {
  !contains( DEFINES, CONFDB_DUMMY ) {
    # *IMPORTANT*: provided Oracle OCCI 19.19.0.0 does not work without this:
    # (NB: CANopen library and CAN interface library must match this)
    QMAKE_CXXFLAGS += -D_GLIBCXX_USE_CXX11_ABI=0 -Wno-narrowing

    ORACLE_DIR = /sw/atlas/sw/lcg/releases/LCG_104c/oracle/19.19.0.0.0/x86_64-el9-gcc13-opt
    #ORACLE_DIR  = /cvmfs/sft.cern.ch/lcg/releases/LCG_104c/oracle/19.19.0.0.0/x86_64-el9-gcc13-opt
    #LIBS += -L/usr/lib/oracle/12.2/client64/lib
    LIBS += -L$${ORACLE_DIR}/lib
    # NB: on Almalinux9 (seudre.nikhef.nl) had to run "yum install libnsl"
    LIBS += -locci
    LIBS += -lclntsh
    LIBS += -lnnz19
    LIBS += $${ORACLE_DIR}/lib/libclntshcore.so.19.1
    INCLUDEPATH += $${ORACLE_DIR}/include
    #INCLUDEPATH += /usr/include/oracle/12.2/client64
  }
}
win32 {
  LIBS += -L/opt/instantclient_19_21/sdk/lib/msvc
  LIBS += -loraocci19
  INCLUDEPATH += /opt/instantclient_19_21/sdk/include
  LIBS += -lWs2_32
}

INCLUDEPATH += $${CANOPEN_DIR}
INCLUDEPATH += ../common

FORMS   += sCanBus.ui

SOURCES += main.cpp
SOURCES += sCanBusDialog.cpp
SOURCES += kernel.cpp
SOURCES += mdm.cpp
SOURCES += ../common/configFile.cpp
!contains( DEFINES, CONFDB_DUMMY ) {
  SOURCES += ../common/confDb.cpp
}

HEADERS += sCanBusDialog.h
HEADERS += kernel.h
HEADERS += mdm.h
HEADERS += ../common/configFile.h
HEADERS += ../common/confDb.h
HEADERS += ../common/mdt_obj_dict.h
HEADERS += $${CANOPEN_DIR}/CanInterface.h
HEADERS += $${CANOPEN_DIR}/CanNode.h
HEADERS += $${CANOPEN_DIR}/CanMessage.h
HEADERS += $${CANOPEN_DIR}/CANopen.h
