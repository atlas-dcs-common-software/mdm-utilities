/*
 *  main ------
 *  Starting point of the sCanBus application.
 *  November 2014, Robert.Hart@nikhef.nl
 */

#include <QApplication>

#include "sCanBusDialog.h"

int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );

  sCanBusDialog scanbusdialog;
  scanbusdialog.show();
  return app.exec();
}
