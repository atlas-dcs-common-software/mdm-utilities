/*
 *  mdm.cpp ------
 *  Implementation of class mdm.
 */

#include <iostream>
#include "mdm.h"

mdm::mdm(const std::string sSerial, const int dId, const std::string sVersion)
{
    _sSerial       = sSerial;
    _dId           = dId;
    _sVersion      = sVersion;

    _bChamberFound = false;
    _sChamber      = "?";
    _dBmask        = -1;
    _sBsens0       = "-";
    _sBsens1       = "-";
    _sBsens2       = "-";
    _sBsens3       = "-";
    _bConfigFound  = false;
    _sDbSerial     = "----";
    _sDbType       = "?";
    _dDbId         = -1;
    _nDbTsens      = -1;
    _nDbMezz       = -1;
    _dDbBmask      = -1;
    _dDbMezzMask   = -1;
}

mdm::~mdm()
{
}

void mdm::setChamber(const bool bChamberFound, const std::string sChamber)
{
    _bChamberFound = bChamberFound;
    _sChamber      = sChamber;
}

void mdm::setBfield(const int dBmask,
                    const std::string sBsens0, const std::string sBsens1,
                    const std::string sBsens2, const std::string sBsens3)
{
    _dBmask  = dBmask;
    _sBsens0 = sBsens0;
    _sBsens1 = sBsens1;
    _sBsens2 = sBsens2;
    _sBsens3 = sBsens3;
}

void mdm::setConfig(const bool bConfigFound,  const std::string sDbSerial, const std::string sDbType,
                    const int dDbId, const int nDbTsens, const int nDbMezz, const int dDbBmask, const int dDbMezzMask)
{
    _bConfigFound = bConfigFound;
    _sDbSerial    = sDbSerial;
    _sDbType      = sDbType;
    _dDbId        = dDbId;
    _nDbTsens     = nDbTsens;
    _nDbMezz      = nDbMezz;
    _dDbBmask     = dDbBmask;
    _dDbMezzMask  = dDbMezzMask;
}

