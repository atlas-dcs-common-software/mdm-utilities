/*
 *  sCanBusDialog.cpp ------
 *  The GUI part of the sCanBus application.
 *  Author: Robert.Hart@nikhef.nl
 *  Summer 2015, moved to Linux SLC6
 */

#include <QFile>
#include <QImage>
#include <QLabel>
#include <QMessageBox>
#include <QTextEdit>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QDateTime>

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "sCanBusDialog.h"
#include "CanInterface.h"
#include "kernel.h"

// Setting variables and defaults
const   QString gsOrganization( "Nikhef"    );
const   QString gsApplication ( "sCanBus"   );
const   QString gsVarIntfType ( "intfType"  );
const   QString gsVarPortIndex( "portIndex" );
const   QString gsVarBaudrate ( "baudrate"  );
const   QString gsDfltIntfType( "KVASER"    );

const   int     giDfltPortIndex = 0;
const   int     giDfltBaudrate  = 125;

bool    gbStopThread;
bool    gbScanAll;

// -----------------------------------------------------------------------------

// Constructor
sCanBusDialog::sCanBusDialog() : QDialog()
{
  setupUi( this );

  gbStopThread = false;
  gbScanAll    = false;

  Qt::WindowFlags flags = windowFlags();
  flags |= Qt::MSWindowsFixedSizeDialogHint;
  flags |= Qt::WindowMinimizeButtonHint;
  setWindowFlags(flags);

  cbInterface->clear();
  cbInterface->addItem( "KVASER" );
#ifdef WIN32
  cbInterface->addItem( "SYSTEC" );
  cbInterface->addItem( "PEAK"   );
  cbInterface->addItem( "NICAN"  );
#else
  cbInterface->addItem( "Socket" );
#endif // WIN32

  cbBaudrate->clear();
  cbBaudrate->addItem( QString::number(10) );
  cbBaudrate->addItem( QString::number(20) );
  cbBaudrate->addItem( QString::number(50) );
  cbBaudrate->addItem( QString::number(125) ); // default
  cbBaudrate->addItem( QString::number(250) );
  cbBaudrate->addItem( QString::number(500) );
  cbBaudrate->addItem( QString::number(800) );
  cbBaudrate->addItem( QString::number(1000) );

  this->readAppSettings();

  connect(butQuit,     SIGNAL(clicked()),                           this, SLOT(onQuit()));
  connect(butOpen,     SIGNAL(clicked()),                           this, SLOT(onOpen()));
  connect(butScan,     SIGNAL(clicked()),                           this, SLOT(onScan()));
  connect(butScanNext, SIGNAL(clicked()),                           this, SLOT(onScanNext()));
  connect(butScanAll,  SIGNAL(clicked()),                           this, SLOT(onScanAll()));
  connect(cbLogfile,   SIGNAL(clicked()),                           this, SLOT(onLogfile()));
  connect(cbInterface, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onInterface(const QString&)));
  connect(rbCommon,    SIGNAL(clicked()),                           this, SLOT(onModeCommon()));
  connect(rbBsensor,   SIGNAL(clicked()),                           this, SLOT(onModeBsensor()));
  connect(rbSettings,  SIGNAL(clicked()),                           this, SLOT(onModeSettings()));

  butScan->setEnabled(false);
  butScanNext->setEnabled(false);
  butScanAll->setEnabled(false);

  _pKernel = new kernel(this);

  connect(_pKernel, SIGNAL(sigLogEdit(const QString&)),     this, SLOT(upd_Stdout(const QString&)));
  connect(_pKernel, SIGNAL(sigOpenCloseStatus(const bool)), this, SLOT(upd_OpenCloseStatus(const bool)));
  connect(_pKernel, SIGNAL(sigWindowTitle(const QString&)), this, SLOT(upd_WindowTitle(const QString&)));
  connect(_pKernel, SIGNAL(sigPortNumber(const int)),       this, SLOT(upd_PortNumber(const int)));
  connect(_pKernel, SIGNAL(sigEndScanAll()),                this, SLOT(upd_EndScanAll()));

  if (_pKernel->initKernel() == false)
    {
      upd_Stdout("FATAL: initKernel() failed");
      return;
    }

  _pKernel->start();

  rbCommon->setChecked(true);    // default output mode
  onModeCommon();                // inform thread about current mode setting

  cbLogfile->setEnabled(true);
  cbLogfile->setChecked(false);  // output to logfile by default off
  onLogfile();                   // inform thread about output to logfile mode

  emit sigPortCount(cbPort->count());
}

// Destructor
sCanBusDialog::~sCanBusDialog()
{
  this->cleanUp();
}

void sCanBusDialog::onQuit()
{
  this->cleanUp();
  this->writeAppSettings();
  exit(0);
}

void sCanBusDialog::cleanUp()
{
  gbStopThread = true;
  if (_pKernel->getStatus() == true)
    {
      _pKernel->wait();
      delete _pKernel;
    }
}

void sCanBusDialog::onOpen()
{
  QString sInterface = cbInterface->currentText();
  int     iPortNo    = cbPort->currentText().toInt();
  int     iBaudrate  = cbBaudrate->currentText().toInt();

  emit sigOpenClose(sInterface, _dInterfaceType, iPortNo, iBaudrate);
}

void sCanBusDialog::onScan()
{
  emit sigScan();
}

void sCanBusDialog::onScanNext()
{
  emit sigScanNext();
}

void sCanBusDialog::onScanAll()
{
  butQuit->setEnabled(gbScanAll);
  butOpen->setEnabled(gbScanAll);
  butScan->setEnabled(gbScanAll);
  butScanNext->setEnabled(gbScanAll);
  rbCommon->setEnabled(gbScanAll);
  rbBsensor->setEnabled(gbScanAll);
  rbSettings->setEnabled(gbScanAll);

  if (gbScanAll == true)
    {
      gbScanAll = false;
      butScanAll->setText("Scan all ports");
      butScanAll->setEnabled(false);
    }
  else
    {
      gbScanAll = true;
      butScanAll->setText("Stop total scan");
      emit sigScanAll();
    }
}

void sCanBusDialog::onInterface(const QString& sIntfType)
{
  cbPort->clear();  // clear the port combobox

  _dInterfaceType = -1;
  if (sIntfType == QString("KVASER"))
    _dInterfaceType = KVASER_INTF_TYPE;
  else if (sIntfType == QString("SYSTEC"))
    _dInterfaceType = SYSTEC_INTF_TYPE;
  else if (sIntfType == QString("PEAK"))
    _dInterfaceType = PEAK_INTF_TYPE;
  else if (sIntfType == QString("NICAN"))
    _dInterfaceType = NICAN_INTF_TYPE;
  else if (sIntfType == QString("Socket"))
    _dInterfaceType = SOCKET_INTF_TYPE;

  // determine the number of ports available of the seclected type
  int  no_of_ports  = 0;
  int* port_numbers = 0;

  no_of_ports = CanInterface::canInterfacePorts(_dInterfaceType, &port_numbers);

  if (_dInterfaceType < 0)
    no_of_ports = 0;

  // fill the Port combobox
  for(int i = 0; i < no_of_ports; ++i)
    this->cbPort->addItem( QString::number(port_numbers[i]) );

  bool bEnabled = (no_of_ports > 0 ? true : false);
  butOpen->setEnabled(bEnabled);
  cbBaudrate->setEnabled(bEnabled);
}

void sCanBusDialog::onModeCommon()
{
  emit sigModeCommon();
}

void sCanBusDialog::onModeBsensor()
{
  emit sigModeBsensor();
}

void sCanBusDialog::onModeSettings()
{
  emit sigModeSettings();
}

void sCanBusDialog::onLogfile()
{
  bool bLogfile = false;

  if (cbLogfile->isChecked() == true)
    bLogfile = true;
  emit sigLogfile(bLogfile);
}

void sCanBusDialog::upd_Stdout(const QString& sText)
{
  logStdout->append(sText);
  logStdout->ensureCursorVisible();
  QCoreApplication::processEvents();
}

void::sCanBusDialog::upd_OpenCloseStatus(const bool bOpen)
{
  if (bOpen == true)
    butOpen->setText("Open");
  else
    butOpen->setText("Close");

  butScan->setDisabled(bOpen);
  butScanNext->setDisabled(bOpen);
  butScanAll->setDisabled(bOpen);

  cbInterface->setEnabled(bOpen);
  cbPort->setEnabled(bOpen);
  cbBaudrate->setEnabled(bOpen);
}

void::sCanBusDialog::upd_WindowTitle(const QString& sTitle)
{
  setWindowTitle(sTitle);
}

void::sCanBusDialog::upd_PortNumber(const int iPort)
{
  cbPort->setCurrentIndex(iPort);
  QCoreApplication::processEvents();
}

void::sCanBusDialog::upd_EndScanAll()
{
  gbScanAll = false;
  butScanAll->setText("Scan all ports");
  butQuit->setEnabled(true);
  butOpen->setEnabled(true);
  butScan->setEnabled(true);
  butScanNext->setEnabled(true);
  butScanAll->setEnabled(true);
  rbCommon->setEnabled(true);
  rbBsensor->setEnabled(true);
  rbSettings->setEnabled(true);
}

void sCanBusDialog::readAppSettings()
{
  int         iIndex;
  int         iPort;
  int         iBaudrate;
  QString     sIntfType;
  QString     sBaudrate;
  QSettings   settings( gsOrganization, gsApplication );

  sIntfType = settings.value( gsVarIntfType, gsDfltIntfType ).toString();
  iIndex = cbInterface->findText( sIntfType );
  cbInterface->setCurrentIndex( iIndex );
  // call onInterface in order to fill the Port combobox
  this->onInterface( sIntfType );

  iPort = settings.value( gsVarPortIndex, giDfltPortIndex ).toInt();
  if (iPort >= 0 && iPort < cbPort->count())
    cbPort->setCurrentIndex( iPort );

  iBaudrate = settings.value( gsVarBaudrate, giDfltBaudrate ).toInt();
  sBaudrate = QString::number( iBaudrate );
  iIndex = cbBaudrate->findText( sBaudrate );
  cbBaudrate->setCurrentIndex( iIndex );
}

void sCanBusDialog::writeAppSettings()
{
  int         iPort;
  QSettings   settings( gsOrganization, gsApplication );

  settings.setValue( gsVarIntfType, cbInterface->currentText());
  iPort = cbPort->currentIndex();
  if (iPort >= 0)
    settings.setValue( gsVarPortIndex, iPort);
  settings.setValue( gsVarBaudrate, cbBaudrate->currentText().toInt() );
}
