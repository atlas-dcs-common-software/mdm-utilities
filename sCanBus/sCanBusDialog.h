/*
 *  sCanBusDialog.h ------
 */

#ifndef SCANBUSDIALOG_H
#define SCANBUSDIALOG_H

#include <QDialog>

#include "ui_sCanBus.h"

/*
#ifndef WIN32
#error WIN32 not set!
#endif

#ifndef WIN64
#error WIN64 not set!
#endif
*/

// Henk: huh? what's this?:
//#ifdef UNICODE
//#error UNICODE set!
//#endif

// forward declarations
class kernel;

class sCanBusDialog: public QDialog, public Ui_Dialog
{
    Q_OBJECT

public:
                sCanBusDialog();
               ~sCanBusDialog();
private:
    kernel*     _pKernel;
    int         _dInterfaceType;

    void        cleanUp();
    void        readAppSettings();
    void        writeAppSettings();

private slots:
    void        onQuit();
    void        onOpen();
    void        onScan();
    void        onScanNext();
    void        onScanAll();
    void        onLogfile();
    void        onInterface(const QString &sText);
    void        onModeCommon();
    void        onModeBsensor();
    void        onModeSettings();

public slots:
    void        upd_Stdout(const QString& sText);
    void        upd_OpenCloseStatus(const bool bOpen);
    void        upd_WindowTitle(const QString& sTitle);
    void        upd_PortNumber(const int iPort);
    void        upd_EndScanAll();

signals:
    void        sigOpenClose(const QString& sInterfaceType, const int dInterfacetype, const int iPortNo, const int iBaudrate);
    void        sigScan();
    void        sigScanNext();
    void        sigScanAll();
    void        sigModeCommon();
    void        sigModeBsensor();
    void        sigModeSettings();
    void        sigLogfile(const bool bLogfile);
    void        sigPortCount(const int iPortCount);
};

#endif
