//
//  confDb.cpp ------
//
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <cctype>
#include "confDb.h"
#include "configFile.h"

#define CONFDB_SET          "ConfDb"
#define CONFDB_USERNAME     "ConfDbUser"
#define CONFDB_PASSWORD     "ConfDbPwd"
#define CONFDB_DATABASE     "ConfDbDbase"
#define CONFDB_ACCOUNT      "ConfDbAccount"

// Default ConfDb parameters
#define CONFDB_D_USERNAME   "ATLAS_CONF_MDT_R"
#define CONFDB_D_PASSWORD   "ReadC0nf"
#define CONFDB_D_DATABASE   "ATONR_CONF"
#define CONFDB_D_ACCOUNT    "atlas_conf_mdt"

confDb::confDb(const std::string& sIniFile)
{
  _bStatus = false;
  _sError  = "";

  configFile* pConfig = new configFile();

  _sDatabase = pConfig->getStringVal(sIniFile, std::string(CONFDB_DATABASE),
                                     std::string(CONFDB_D_DATABASE));
  _sUsername = pConfig->getStringVal(sIniFile, std::string(CONFDB_USERNAME),
                                     std::string(CONFDB_D_USERNAME));
  _sPassword = pConfig->getStringVal(sIniFile, std::string(CONFDB_PASSWORD),
                                     std::string(CONFDB_D_PASSWORD));
  _sAccount  = pConfig->getStringVal(sIniFile, std::string(CONFDB_ACCOUNT),
                                     std::string(CONFDB_D_ACCOUNT));

  _pEnv = Environment::createEnvironment(Environment::DEFAULT);

  try
    {
      // Open a connection to the database
      _pCon    = _pEnv->createConnection( _sUsername, _sPassword, _sDatabase );
      _bStatus = true;
    }
  catch( SQLException &ea )
    {
      _sError  = ea.what();
      _bStatus = false;
    }
}

confDb::~confDb()
{
  if (_bStatus == true)
    _pEnv->terminateConnection( _pCon );
  Environment::terminateEnvironment( _pEnv );
}

bool confDb::wrIniUsedFile(const std::string& sIniFile)
{
  FILE    *fp;
  fp = fopen(sIniFile.c_str(), "a+");
  if (fp == NULL)
    return false;

  fprintf(fp, "[%s]\n",      CONFDB_SET);
  fprintf(fp, "%-14s= %s\n", CONFDB_DATABASE, _sDatabase.c_str());
  fprintf(fp, "%-14s= %s\n", CONFDB_USERNAME, _sUsername.c_str());
  fprintf(fp, "%-14s= %s\n", CONFDB_PASSWORD, _sPassword.c_str());
  fprintf(fp, "%-14s= %s\n", CONFDB_ACCOUNT,  _sAccount.c_str());
  fprintf(fp, "\n");

  fclose(fp);
  return true;
}

bool confDb::showCanbusList()
{
  if (_bStatus == false)
    return false;

  bool        bResult;
  std::string sCmdSQL;
  sCmdSQL  = "SELECT * FROM TABLE(";
  sCmdSQL += _sAccount;
  sCmdSQL += ".canbus.get_pc_for_canbus(null))";
  try
    {
      Statement   *s;
      ResultSet   *r;
      std::string s1, s2, s3;

      s = _pCon->createStatement(sCmdSQL.c_str());
      r = s->executeQuery();

      while(r->next() == ResultSet::DATA_AVAILABLE)
        {
          s1 = r->getString(1);
          s2 = r->getString(2);
          s3 = r->getString(3);

          std::string sResult = "";
          for(unsigned int i = 0; i < s1.length(); ++i)
            sResult += std::toupper(s1[i]);
          std::cout << sResult << ", " << s2 << ", " << s3 << std::endl;
        }
      s->closeResultSet(r);
      _pCon->terminateStatement(s);
      bResult = true;
    }
  catch( SQLException &ea )
    {
      _sError = ea.what();
      bResult = false;
    }

  return bResult;
}

bool confDb::getCanbus( const std::string& sHost,
                        const int          dPort,
                        std::string&       sCanbus)
{
  sCanbus = "?";

  if (_bStatus == false)
    return false;

  unsigned int    i;
  char            cBuf[32];
  std::string     sUpperHost;
  std::string     sPort;

  sUpperHost = "";
  for(i = 0; i < sHost.length(); ++i)
    sUpperHost += std::toupper(sHost[i]);

  sprintf(cBuf, "%d", dPort+1);
  sPort = cBuf;

  bool        bResult;
  std::string sCmdSQL;
  sCmdSQL  = "SELECT * FROM TABLE(";
  sCmdSQL += _sAccount;
  sCmdSQL += ".canbus.get_pc_for_canbus(null))";
  try
    {
      Statement   *s;
      ResultSet   *r;
      std::string sTmpHost;
      std::string s1, s2, s3;
      bool        bFound = false;
      s = _pCon->createStatement(sCmdSQL.c_str());
      r = s->executeQuery();
      while(r->next() == ResultSet::DATA_AVAILABLE)
        {
          s1 = r->getString(1);
          s2 = r->getString(2);
          s3 = r->getString(3);

          sTmpHost = "";
          for(i = 0; i < s1.length(); ++i)
            sTmpHost += std::toupper(s1[i]);
          if (sUpperHost == sTmpHost && sPort == s2)
            {
              bFound = true;
              sCanbus = s3;
              break;
            }
        }
      s->closeResultSet(r);
      _pCon->terminateStatement(s);
      bResult = bFound;
    }
  catch( SQLException &ea )
    {
      _sError = ea.what();
      bResult = false;
    }

  return bResult;
}

bool confDb::getChamber( const std::string& sSerial,
                         bool&              bFound,
                         std::string&       sChamber)
{
  bFound   = false;
  sChamber = "?";

  if (_bStatus == false)
    return false;

  bool        bResult;
  std::string sCmdSQL;
  sCmdSQL  = "SELECT * FROM TABLE(";
  sCmdSQL += _sAccount;
  sCmdSQL += ".canbus.get_mdt_for_mdm('";
  sCmdSQL += sSerial;
  sCmdSQL += "'))";
  try
    {
      Statement   *s;
      ResultSet   *r;
      s = _pCon->createStatement(sCmdSQL.c_str());
      r = s->executeQuery();
      while(r->next() == ResultSet::DATA_AVAILABLE)
        {
          std::string s1 = r->getString(1);
          std::string s2 = r->getString(2);
          if (s1 == sSerial)
            {
              sChamber = s2;
              bFound   = true;
              break;
            }
        }
      s->closeResultSet(r);
      _pCon->terminateStatement(s);
      bResult = true;
    }
  catch( SQLException &ea )
    {
      _sError = ea.what();
      bResult = false;
    }

  return bResult;
}

bool confDb::getConfig( const std::string& sChamber,
                        bool&              bFound,
                        std::string&       sSerial,
                        std::string&       sType,
                        int &              dId,
                        int &              nTsens,
                        int &              nMezz,
                        int &              dBmask,
                        int &              dMezzMask )
{
  // presets
  bFound    = false;
  sSerial   = "?";
  sType     = "-";
  dId       = -1;
  nTsens    = -1;
  nMezz     = -1;
  dBmask    = -1;
  dMezzMask = -1;

  if (_bStatus == false)
    return false;

  bool        bResult;
  std::string sCmdSQL;
  sCmdSQL  = "SELECT * FROM TABLE(";
  sCmdSQL += _sAccount;
  sCmdSQL += ".canbus.get_canbus_params('";
  sCmdSQL += sChamber;
  sCmdSQL += "'))";
  try
    {
      Statement   *s;
      ResultSet   *r;
      s = _pCon->createStatement(sCmdSQL.c_str());
      r = s->executeQuery();
      while(r->next() == ResultSet::DATA_AVAILABLE)
        {
          std::string s1 = r->getString(1);
          std::string s2 = r->getString(2);
          std::string s3 = r->getString(3);
          if (s1 == sChamber)
            {
              bFound = true;
              if (s2 == "mdm")
                sSerial   = s3;
              else if (s2 == "type")
                sType     = s3;
              else if (s2 == "node")
                dId       = atoi((const char*)s3.c_str()); 
              else if (s2 == "n_tsen")
                nTsens    = atoi((const char*)s3.c_str());
              else if (s2 == "n_mezz")
                nMezz     = atoi((const char*)s3.c_str());
              else if (s2 == "mask_bsen")
                dBmask    = atoi((const char*)s3.c_str());
              else if (s2 == "csm_enable")
                dMezzMask = atoi((const char*)s3.c_str());
            }
        }
      s->closeResultSet(r);
      _pCon->terminateStatement(s);
      bResult = true;
    }
  catch( SQLException &ea )
    {
      _sError = ea.what();
      bResult = false;
    }

  return bResult;
}
