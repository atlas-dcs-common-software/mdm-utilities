/*
 * configFile.h ------
 */
#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#define MAX_LINE 1024

class configFile
{
public:
                configFile();
               ~configFile();

    bool        chkIniFile  (const std::string& sIniFile);
    int         getIntVal   (const std::string& sIniFile, const std::string& sParam, const int          dDefault);
    float       getFloatVal (const std::string& sIniFile, const std::string& sParam, const float        fDefault);
    std::string getStringVal(const std::string& sIniFile, const std::string& sParam, const std::string& sDefault);
};

#endif // CONFIGFILE_H
