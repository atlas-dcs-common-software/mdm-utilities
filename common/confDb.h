//
//  confDb.h ------
//
#ifndef CONFDB_H
#define CONFDB_H

#ifndef CONFDB_DUMMY

#include <occi.h>
#include <string>
using namespace oracle::occi;

class confDb
{
public:
                    confDb(const std::string& sIniFile);
                   ~confDb();

    bool            getStatus()   { return _bStatus;   };
    std::string     getError()    { return _sError;    };
    std::string     getDatabase() { return _sDatabase; };
    std::string     getUsername() { return _sUsername; };
    std::string     getPassword() { return _sPassword; };
    std::string     getAccount()  { return _sAccount;  };

    bool            showCanbusList();
    bool            getCanbus (const std::string& sHost, const int dPort, std::string& sCanbus);
    bool            getChamber(const std::string& sSerial,  bool& bFound, std::string& sChamber);
    bool            getConfig (const std::string& sChamber, bool& bFound, std::string& sSerial, std::string& sType,
                               int &dId, int &nTsens, int &nMezz, int &dBmask, int &dMezzMask);
    bool            wrIniUsedFile(const std::string& sIniFile);
private:
    bool            _bStatus;
    std::string     _sError;

    std::string     _sDatabase;
    std::string     _sUsername;
    std::string     _sPassword;
    std::string     _sAccount;

    Environment*    _pEnv;
    Connection*     _pCon;
};

#else // CONFDB_DUMMY

#include <string>
class confDb
{
public:
                    confDb(const std::string& ) { };
                   ~confDb() { };

    bool            getStatus()   { return false; };
    std::string     getError()    { return std::string("<confDb: dummy class>"); };
    std::string     getDatabase() { return std::string(""); };
    std::string     getUsername() { return std::string(""); };
    std::string     getPassword() { return std::string(""); };
    std::string     getAccount()  { return std::string(""); };

    bool            showCanbusList() { return false; }
    bool            getCanbus (const std::string& , const int , std::string& ) { return false; }
    bool            getChamber(const std::string& ,  bool& , std::string& ) { return false; }
    bool            getConfig (const std::string& , bool& , std::string& , std::string& ,
                               int &, int &, int &, int &, int &) { return false; }
    bool            wrIniUsedFile(const std::string& ) { return false; }
};

#endif // CONFDB_DUMMY

#endif // CONFDB_H
