/*
 *  mdt_obj_dict.h ------
 *  MDT-DCS Object Dictionary
 *  Meant for the MDT-DCS-Module (MDM)
 */

#ifndef MDT_OBJ_DICT_H
#define MDT_OBJ_DICT_H

//
// Object Dictionary definitions
//
// General ELMB definitions
#define OD_SCAN                                 0x1000
#define OD_EEPROM                               0x1010
#define OD_VERSION                              0x100A
#define OD_SERIAL                               0x3100
#define OD_LIFE_GUARDING                        0x100D
#define OD_TXPDO1                               0x1800
#define OD_TXPDO2                               0x1801
#define OD_TXPDO3                               0x1802
#define OD_TXPDO4                               0x1803
// ELMB-ADC definitions
#define OD_ELMB_ADC                             0x2100
#define OD_ELMB_ADC_RESET                       0x2200
#define OD_ELMB_ADC_ENABLED                     0x2400
#define OD_ELMB_ADC_CALIBRATE                   0x2300
#define OD_ELMB_ADC_RAWDATA                     0x4401
#define OD_ELMB_ADC_NTCCONV                     0x4400
// CSM-ADC definitions
#define OD_CSM_ADC                              0x2101
#define OD_CSM_ADC_RESET                        0x2201
#define OD_CSM_ADC_ENABLED                      0x2401
#define OD_CSM_ADC_CALIBRATE                    0x2301
// BFIELD definitions
#define OD_BF_NSENSORS                          0x2800
#define OD_BF_ADC_CALIBRATE                     0x2700
#define OD_BF_ADC_B0                            0x2500
#define OD_BF_ADC_B1                            0x2501
#define OD_BF_ADC_B2                            0x2502
#define OD_BF_ADC_B3                            0x2503
#define OD_BF_ADC_B0_RESET                      0x2600
#define OD_BF_ADC_B1_RESET                      0x2601
#define OD_BF_ADC_B2_RESET                      0x2602
#define OD_BF_ADC_B3_RESET                      0x2603
#define OD_BF_ID0                               0x2900
#define OD_BF_ID1                               0x2901
#define OD_BF_ID2                               0x2902
#define OD_BF_ID3                               0x2903
// DIG-IO definitions
#define OD_INIT_DIG_OUTPUT                      0x2F00
#define OD_DIGIO_INPUT                          0x6000
#define OD_DIGIO_ENABLED                        0x6005
#define OD_DIGIO_OUTPUT                         0x6200
#define OD_OUTPUT_BIT                           0x6220

//
// Sub-Index definitions
//
#define SI_CHANNELS                             1
#define SI_RATE                                 2
#define SI_RANGE                                3
#define SI_MODE                                 4
#define SI_REFCHAN                              19
#define SI_MEZZMASK                             22
#define SI_HALL_RATE                            2
#define SI_HALL_RANGE                           3
#define SI_HALL_MODE                            4
#define SI_TEMP_RATE                            5
#define SI_TEMP_RANGE                           6
#define SI_TEMP_MODE                            7
#define SI_OFFSET_H1                            10
#define SI_OFFSET_H2                            12
#define SI_OFFSET_H3                            14
#define SI_OFFSET_TEMP                          16
#define SI_GAIN_H1                              11
#define SI_GAIN_H2                              13
#define SI_GAIN_H3                              15
#define SI_GAIN_TEMP                            17

// NTC Conversion definitions
#define NTC_RESISTOR                            0
#define NTC_MDEGREES                            1

// DIG-IO constants
#define DIG_IO_BITS                             7
#define DIG_OUTPUT_BITS                         4

//
// Default values
// 
// PDO defaults
#define D_PDO1                                  false
#define D_PDO2                                  true
#define D_PDO3                                  true
#define D_PDO4                                  true
// DIG-IO defaults
#define D_DIGOUTPUT                             1
#define D_DIGOUTPUT_CSM4                        9
#define D_INIT_DIG_OUTPUT                       0x9
// ELMB-ADC defaults
#define D_ELMB_ADC_ENABLED                      true
#define D_ELMB_ADC_RESETCAL                     false
#define D_ELMB_ADC_RAWDATA                      false
#define D_ELMB_ADC_RATE                         0
#define D_ELMB_ADC_RANGE                        5
#define D_ELMB_ADC_MODE                         1
#define D_ELMB_ADC_CHANNELS                     60
#define D_ELMB_ADC_CONVERSION                   NTC_MDEGREES
// CSM-ADC defaults
#define D_CSM_ADC_ENABLED                       true
#define D_CSM_ADC_RESETCAL                      false
#define D_CSM_ADC_RATE                          0
#define D_CSM_ADC_RANGE                         4
#define D_CSM_ADC_MODE                          0
#define D_CSM_ADC_CHANNELS                      64
#define D_CSM_ADC_REFCHAN                       11
// BFIELD defaults
#define D_BF_ADC_NSENSORS                       0
#define D_BF_ADC_RESETCAL                       false
#define D_BF_ADC_HALL_RATE                      0
#define D_BF_ADC_HALL_RANGE                     0
#define D_BF_ADC_HALL_MODE                      0
#define D_BF_ADC_TEMP_RATE                      0
#define D_BF_ADC_TEMP_RANGE                     5
#define D_BF_ADC_TEMP_MODE                      1

// NMT definitions
#define NMT_STOP                                0x2
#define NMT_START                               0x1
#define NMT_RESET                               0x81

//
// Emergency Object definitions
//
// Error Codes (byte 0-1)
#define EC_DEVICE_HARDWARE                      0x5000
#define EC_DEVICE_SOFTWARE                      0x6000
#define EC_CAN_COMMUNICATION                    0x8100
#define EC_LIFEGUARD_ERROR                      0x8130
#define EC_PROTOCOL_ERROR                       0x8200
#define EC_PDO_LENGTH_ERROR                     0x8210
// Error Field (byte3)
#define EF_NTC_CONVERSION_TO                    0x01
#define EF_NTC_RESET                            0x02
#define EF_NTC_OFFSET_CALIB                     0x03
#define EF_NTC_GAIN_CALIB                       0x04
#define EF_NTC_INIT                             0x05
#define EF_CRC                                  0x30
#define EF_EEPROM_WRITE                         0x41
#define EF_EEPROM_READ                          0x42
#define EF_BF_CONVERSION_TO                     0x51
#define EF_BF_RESET                             0x52
#define EF_BF_HALL_CALIB                        0x53
#define EF_BF_TEMP_CALIB                        0x54
#define EF_BF_INIT                              0x55
#define EF_CSM_CONVERSION_TO                    0x61
#define EF_CSM_RESET                            0x62
#define EF_CSM_OFFSET_CALIB                     0x63
#define EF_CSM_GAIN_CALIB                       0x64
#define EF_CSM_INIT                             0x65
#define EF_JTAG_SHIFT_PROTO                     0x71
#define EF_JTAG_SEQ_PROTO                       0x72
#define EF_JTAG_ACTION_AVAIL                    0x81
#define EF_JTAG_ACTION_CRC                      0x82
#define EF_JTAG_ACTION_STATUS                   0x83
#define EF_IRREGULAR_RESET                      0xf0
#define EF_BL_NOTPRESENT                        0xf1
#define EF_BL_INCONTROL                         0xfe

#endif  // MDT_OBJ_DICT_H

