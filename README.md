# MDM-utilities

Contains a couple of MDM 'utilities' (GUIs based on the Qt toolkit) to be used on
pcatlmdtmdm[1-6] or on pcatlmdtmdm7,<br>
using CANspy to connect to any of the MDM CAN buses<br>
(NB: on pcatlmdtmdm7 currently the *CANspy* bus is connected to Systec port 2,
the selected *MDM* bus to Systec port 3).

- **MdtElmb**
  Application to configure MDM-specific settings.
- **sCanBus**
  Application to scan the CAN bus for connected MDMs,
  and show their expected configuration as retrieved from the database, if any.

To compile (Feb 2024):
- logon to the machine vm-tdq-build-02 (AlmaLinux9)
- `source /sw/tdaq/setup/setup_tdaq-11-02-00.sh`
- in the top directory MDM-utilities: <br>
  **NB** this potentially does not work in some terminal windows ("could not load platform plugin xcb"): <br>
  `/sw/atlas/sw/lcg/releases/LCG_104c/qt5/5.15.9/x86_64-el9-gcc13-opt/bin/qmake -r` <br>
  but this then might work: <br>
  `/sw/atlas/sw/lcg/releases/LCG_101/qt5/5.12.4/x86_64-centos7-gcc11-opt/bin/qmake -r`
- **NB** to compile the tools without Oracle client calls run above _qmake_ call like this: <br>
  `<path>/qmake DEFINES+=CONFDB_DUMMY -r` <br>
  (first remove all existing Makefiles to make it work properly...).
- `make`

To run (on any of the pcatlmdtmdm<1-7> machines):
- the executables are in directory MDM-utilities/Release
- **NB** this potentially does not work in some terminal windows ("could not load platform plugin xcb"): <br>
  `source /sw/tdaq/setup/setup_tdaq-11-02-00.sh` <br>
  but this then might work: <br>
  `source /sw/tdaq/setup/setup_tdaq-09-04-00.sh`
- To make sure CANtools' libCANopen is found: <br>
  `export LD_LIBRARY_PATH=<path-to-CANtools>/Release:$LD_LIBRARY_PATH`
- `./MdtElmb'
