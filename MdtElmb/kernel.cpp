/*
 *  kernel.cpp ------
 *  The heart of the MdtElmb application.
 */

#include <iostream>
#include <cstdio>
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "kernel.h"
#include "MdtElmbDialog.h"
#include "CanInterface.h"
#include "CanNode.h"
#include "confDb.h"
#include "configFile.h"
#include "mdt_obj_dict.h"

#define SCANBUS_INI_FILE    "./mdtelmb.ini"

#define BUF_SIZE            1024

#define SETTING_SECTION     "Setting"
#define FIRMWARE            "Firmware"
#define LIFEGUARDING        "LifeGuarding"
#define D_FIRMWARE          "MD25.0001"
#define D_LIFEGUARDING      240

#define MAX_BSENSOR         4

const   QString gsVersion( "v4.1" );
extern  bool    gbStopThread;

// -----------------------------------------------------------------------------

// Constructor
kernel::kernel(MdtElmbDialog* pDialog)
  : _pCanInterface(0)
{
  _bStatus             = false;
  _pMdtElmbDialog      = pDialog;
  _pCanInterface       = NULL;
  _pCanNode            = NULL;
  _pConfDb             = NULL;
  _sHostname           = "?";
  _iPortCount          = -1;
  _dInterfaceType      = -1;
  _iPortNo             = -1;
  _iBaudrate           = -1;
  _iNode               = -1;
  _nNodes              = 0;
  _bReadSettingsFailed = true;

  connect(_pMdtElmbDialog,
          SIGNAL(sigOpenPort(const QString&, const int, const int, const int)),
          this,
          SLOT(upd_OpenPort(const QString&, const int, const int, const int)));

  connect(_pMdtElmbDialog, SIGNAL(sigPortCount(const int)), this, SLOT(upd_PortCount(const int)));
  connect(_pMdtElmbDialog, SIGNAL(sigScan()),               this, SLOT(upd_Scan()));
  connect(_pMdtElmbDialog, SIGNAL(sigReadNode(const int)),  this, SLOT(upd_ReadNode(const int)));

  connect(_pMdtElmbDialog,
          SIGNAL(sigWriteSave(const int, const bool, const bool, const bool, const bool,
                              const int, const int, const int, const int, const int, const bool, const bool,
                              const int, const int, const int, const int, const int, const bool, const bool, const int,
                              const int, const bool,
                              const bool, const int, const bool, const bool, const bool, const bool)),
          this,
          SLOT(upd_WriteSave( const int, const bool, const bool, const bool, const bool,
                              const int, const int, const int, const int, const int, const bool, const bool,
                              const int, const int, const int, const int, const int, const bool, const bool, const int,
                              const int, const bool,
                              const bool, const int, const bool, const bool, const bool, const bool)));
}

// Destructor
kernel::~kernel()
{
  this->cleanUp();
}

void kernel::cleanUp()
{
  if (_pConfDb)
    delete _pConfDb;
  if (_pCanNode)
    delete _pCanNode;
  if (_pCanInterface)
    delete _pCanInterface;

  _pCanInterface = NULL;
  _pCanNode      = NULL;
  _pConfDb       = NULL;
}

bool kernel::initKernel()
{
  if (_bStatus == true)
    return true;

#ifdef WIN32
  // Initialize Winsock
  WSADATA wsadata;
  int err = WSAStartup( 0x0202, &wsadata );
  if( err != 0 )
    log( QString::asprintf("ERROR: WSAStartup failed, error code %d",err) );
  else if( wsadata.wVersion != 0x0202 )
    log( QString::asprintf("ERROR: Winsock version: 0x%X", wsadata.wVersion) );
#endif // WIN32

  std::string sIniFile = SCANBUS_INI_FILE;
  configFile* pConfig  = new configFile();
  _sDfltFirmware       = pConfig->getStringVal(sIniFile, FIRMWARE,     D_FIRMWARE);
  _dDfltLifeGuarding   = pConfig->getIntVal   (sIniFile, LIFEGUARDING, D_LIFEGUARDING);

  char cHost[BUF_SIZE];
  if (gethostname(cHost, sizeof(cHost)) == 0)
    _sHostname = cHost;

  _pConfDb = new confDb(sIniFile);
  _bConfDbStat = _pConfDb->getStatus();
  if (_bConfDbStat == false)
    {
      QString sError;
      sError = "WARNING: confDb constructor failed";
      log(sError);
      sError = _pConfDb->getError().c_str();
      log(sError);
    }
  // _pConfDb->showCanbusList();

  wrIniUsedFile(sIniFile);

  setWindowTitle();

  QString sInfo;
  sInfo = QString::asprintf("On host: %s", _sHostname.c_str());
  log(sInfo);
  _bStatus = true;
  return _bStatus;
}

void kernel::run()
{
  if (_bStatus == false)
    return;

  while(gbStopThread == false)
    {
      sleep(1);
      qApp->processEvents();
    }
}

void kernel::upd_OpenPort(const QString& sInterfaceType, const int dInterfaceType, const int iPortNo, const int iBaudrate)
{
  _sInterfaceType = sInterfaceType;
  _dInterfaceType = dInterfaceType;
  _iPortNo        = iPortNo;
  _iBaudrate      = iBaudrate;

  openCloseCanPort(true, sInterfaceType, dInterfaceType, iPortNo, iBaudrate);
}

void kernel::upd_PortCount(const int iPortCount)
{
  _iPortCount = iPortCount;
}

void kernel::upd_Scan()
{
  doScan();
}

void kernel::upd_ReadNode(const int iNode)
{
  doReadNode(iNode);
}

void kernel::upd_WriteSave( const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4,
                            const int nAdcChan, const int dAdcRate, const int dAdcRange, const int dAdcMode,
                            const int dNtcConv, const bool bAdcEnabled, const bool bRawData,
                            const int nCsmChan, const int dCsmRate, const int dCsmRange, const int dCsmMode,
                            const int dRefChan, const bool bCsmEnabled, const bool bCsmCalibrate, const int dMezzMask,
                            const int dBmask, const bool bBfCalibrate,
                            const bool bDigIOEnabled, const int dDigOutput,
                            const bool bDigBit1, const bool bDigBit2, const bool bDigBit3, const bool bDigBit4)
{
  QString      sInfo;
  unsigned int iData;

  if (_iNode < 0 || _bReadSettingsFailed == true)
    {   // redundant check
      sInfo = QString::asprintf("WriteSave( node=%d, readsettingfailed=%s )", _iNode, (_bReadSettingsFailed == false ? "false" : "true"));
      pSeriousError(sInfo);
      return;
    }

  int iNode      = _iNode;
  int nMutations = 0;

  if (dLifeGuard != _dRdLifeGuard)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_LIFE_GUARDING, 0, 1, 200, dLifeGuard) == false)
        return;
      sInfo = QString::asprintf("%02d - LifeGuard    : %d -> %d", nMutations, _dRdLifeGuard, dLifeGuard);
      log(sInfo);
    }
  if (bTxPDO1 != _bRdTxPDO1)
    {
      ++nMutations;
      iData = (bTxPDO1 == true ? 1 : 255);
      if (writeSdoObject(iNode, OD_TXPDO1, 2, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - TxPDO1       : %d -> %d", nMutations, _bRdTxPDO1, bTxPDO1);
      log(sInfo);
    }
  if (bTxPDO2 != _bRdTxPDO2)
    {
      ++nMutations;
      iData = (bTxPDO2 == true ? 1 : 255);
      if (writeSdoObject(iNode, OD_TXPDO2, 2, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - TxPDO2       : %d -> %d", nMutations, _bRdTxPDO2, bTxPDO2);
      log(sInfo);
    }
  if (bTxPDO3 != _bRdTxPDO3)
    {
      ++nMutations;
      iData = (bTxPDO3 == true ? 1 : 255);
      if (writeSdoObject(iNode, OD_TXPDO3, 2, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - TxPDO3       : %d -> %d", nMutations, _bRdTxPDO3, bTxPDO3);
      log(sInfo);
    }
  if (bTxPDO4 != _bRdTxPDO4)
    {
      ++nMutations;
      iData = (bTxPDO4 == true ? 1 : 255);
      if (writeSdoObject(iNode, OD_TXPDO4, 2, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - TxPDO4       : %d -> %d", nMutations, _bRdTxPDO4, bTxPDO4);
      log(sInfo);
    }

  if (nAdcChan != _dRdAdcChan)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_ELMB_ADC, SI_CHANNELS, 1, 200, nAdcChan) == false)
        return;
      sInfo = QString::asprintf("%02d - AdcChan       : %d -> %d", nMutations, _dRdAdcChan, nAdcChan);
      log(sInfo);
    }
  if (dAdcRate != _dRdAdcRate)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_ELMB_ADC, SI_RATE, 1, 200, dAdcRate) == false)
        return;
      sInfo = QString::asprintf("%02d - AdcRate       : %d -> %d", nMutations, _dRdAdcRate, dAdcRate);
      log(sInfo);
    }
  if (dAdcRange != _dRdAdcRange)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_ELMB_ADC, SI_RANGE, 1, 200, dAdcRange) == false)
        return;
      sInfo = QString::asprintf("%02d - AdcRange      : %d -> %d", nMutations, _dRdAdcRange, dAdcRange);
      log(sInfo);
    }
  if (dAdcMode != _dRdAdcMode)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_ELMB_ADC, SI_MODE, 1, 200, dAdcMode) == false)
        return;
      sInfo = QString::asprintf("%02d - AdcMode       : %d -> %d", nMutations, _dRdAdcMode, dAdcMode);
      log(sInfo);
    }
  if (dNtcConv != _dRdNtcConv)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_ELMB_ADC_NTCCONV, 0, 1, 200, dNtcConv) == false)
        return;
      sInfo = QString::asprintf("%02d - NtcConv       : %d -> %d", nMutations, _dRdNtcConv, dNtcConv);
      log(sInfo);
    }
  if (bAdcEnabled != _bRdAdcEnabled)
    {
      ++nMutations;
      iData = (bAdcEnabled == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_ELMB_ADC_ENABLED, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - AdcEnabled    : %d -> %d", nMutations, _bRdAdcEnabled, bAdcEnabled);
      log(sInfo);
    }
  if (bRawData != _bRdRawData)
    {
      ++nMutations;
      iData = (bRawData == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_ELMB_ADC_RAWDATA, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - RawData       : %d -> %d", nMutations, _bRdRawData, bRawData);
      log(sInfo);
    }

  if (nCsmChan != _dRdCsmChan)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_CHANNELS, 1, 200, nCsmChan) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmChan       : %d -> %d", nMutations, _dRdCsmChan, nCsmChan);
      log(sInfo);
    }
  if (dCsmRate != _dRdCsmRate)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_RATE, 1, 200, dCsmRate) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmRate       : %d -> %d", nMutations, _dRdCsmRate, dCsmRate);
      log(sInfo);
    }
  if (dCsmRange != _dRdCsmRange)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_RANGE, 1, 200, dCsmRange) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmRange      : %d -> %d", nMutations, _dRdCsmRange, dCsmRange);
      log(sInfo);
    }
  if (dCsmMode != _dRdCsmMode)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_MODE, 1, 200, dCsmMode) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmMode       : %d -> %d", nMutations, _dRdCsmMode, dCsmMode);
      log(sInfo);
    }
  if (dRefChan != _dRdRefChan)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_REFCHAN, 1, 200, dRefChan) == false)
        return;
      sInfo = QString::asprintf("%02d - RefChan       : %d -> %d", nMutations, _dRdRefChan, dRefChan);
      log(sInfo);
    }
  if (bCsmEnabled != _bRdCsmEnabled)
    {
      ++nMutations;
      iData = (bCsmEnabled == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_CSM_ADC_ENABLED, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmEnabled    : %d -> %d", nMutations, _bRdCsmEnabled, bCsmEnabled);
      log(sInfo);
    }
  if (bCsmCalibrate != _bRdCsmCalibrate)
    {
      ++nMutations;
      iData = (bCsmCalibrate == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_CSM_ADC_CALIBRATE, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - CsmCalibrate  : %d -> %d", nMutations, _bRdCsmCalibrate, bCsmCalibrate);
      log(sInfo);
    }
  if (dMezzMask != _dRdMezzMask)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_CSM_ADC, SI_MEZZMASK, 4, 200, dMezzMask) == false)
        return;
      sInfo = QString::asprintf("%02d - MezzMask      : %d -> %d", nMutations, _dRdMezzMask, dMezzMask);
      log(sInfo);
    }

  if (dBmask != _dRdBmask)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_BF_NSENSORS, 0, 1, 200, dBmask) == false)
        return;
      sInfo = QString::asprintf("%02d - Bmask         : %d -> %d", nMutations, _dRdBmask, dBmask);
      log(sInfo);
    }
  if (bBfCalibrate != _bRdBfCalibrate)
    {
      ++nMutations;
      iData = (bBfCalibrate == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_BF_ADC_CALIBRATE, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - BfCalibrate   : %d -> %d", nMutations, _bRdBfCalibrate, bBfCalibrate);
      log(sInfo);
    }

  if (bDigIOEnabled != _bRdDigIOEnabled)
    {
      ++nMutations;
      iData = (bDigIOEnabled == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_DIGIO_ENABLED, 0, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - DigIOEnabled   : %d -> %d", nMutations, _bRdDigIOEnabled, bDigIOEnabled);
      log(sInfo);
    }
  if (dDigOutput != _dRdDigOutput)
    {
      ++nMutations;
      if (writeSdoObject(iNode, OD_INIT_DIG_OUTPUT, 0, 1, 200, dDigOutput) == false)
        return;
      sInfo = QString::asprintf("%02d - DigOutput      : %d -> %d", nMutations, _dRdDigOutput, dDigOutput);
      log(sInfo);
    }
  if (bDigBit1 != _bRdDigBit1)
    {
      ++nMutations;
      iData = (bDigBit1 == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_OUTPUT_BIT, 1, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - DigBit1        : %d -> %d", nMutations, _bRdDigBit1, bDigBit1);
      log(sInfo);
    }
  if (bDigBit2 != _bRdDigBit2)
    {
      ++nMutations;
      iData = (bDigBit2 == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_OUTPUT_BIT, 2, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - DigBit2        : %d -> %d", nMutations, _bRdDigBit2, bDigBit2);
      log(sInfo);
    }
  if (bDigBit3 != _bRdDigBit3)
    {
      ++nMutations;
      iData = (bDigBit3 == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_OUTPUT_BIT, 3, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - DigBit3        : %d -> %d", nMutations, _bRdDigBit3, bDigBit3);
      log(sInfo);
    }
  if (bDigBit4 != _bRdDigBit4)
    {
      ++nMutations;
      iData = (bDigBit4 == true ? 1 : 0);
      if (writeSdoObject(iNode, OD_OUTPUT_BIT, 4, 1, 200, iData) == false)
        return;
      sInfo = QString::asprintf("%02d - DigBit4        : %d -> %d", nMutations, _bRdDigBit4, bDigBit4);
      log(sInfo);
    }

  if (nMutations > 0)
    {
      if (writeSave(iNode) == false)
        return;
    }
  sInfo = QString::asprintf("WriteSave mutations %d", nMutations);
  log(sInfo);
}

void kernel::openCloseCanPort( const bool     bReportError,
                               const QString& sInterfaceType,
                               const int      dInterfaceType,
                               const int      iPortNo,
                               const int      iBaudrate )
{
  QString sError;

  emit sigCanBus("<not conn>");
  emit sigChamber("");
  emit sigBaseInfo("");
  emit sigSettingWidgets(false);

  if (_pCanInterface)
    {
      // close/delete the Can Node and Interface port object
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = NULL;
      delete _pCanInterface;
      _pCanInterface = NULL;
      emit sigOpenPortStatus(true);
      return;
    }

  _pCanInterface = CanInterface::newCanInterface(dInterfaceType, iPortNo, iBaudrate);

  if (_pCanInterface)
    {
      if (_pCanInterface->opStatus() != 0)
        {
          if (bReportError == true)
            {
              sError  = sInterfaceType;
              sError += " CAN port: \n";
              sError += _pCanInterface->errString().c_str();
              log(sError);
            }
          // Delete the interface object
          delete _pCanInterface;
          _pCanInterface = NULL;
          return;
        }

      // CAN port opened successfully
      if (_pCanNode)
        delete _pCanNode;
      _pCanNode = new CanNode(_pCanInterface, 0);

      emit sigOpenPortStatus(false);

      QString     sInfo, sTmp;
      std::string sCanbus;

      if (_pConfDb->getCanbus(_sHostname, iPortNo, sCanbus) == false)
        sCanbus = "<no name>";

      sInfo = QString("Open CAN bus(%1, %2, %3): %4").arg(sInterfaceType).arg(iPortNo).
        arg(iBaudrate).arg(sCanbus.c_str());
      log(sInfo);
      emit sigCanBus(sCanbus.c_str());
      return;
    }

  if (bReportError == true)
    {
      sError  = sInterfaceType;
      sError += " CAN port: \n";
      sError += "Failed to create the CAN port object";
      log(sError);
    }
}

void kernel::log(const QString& sMess)
{
  emit sigLogEdit(sMess);
}


void kernel::setWindowTitle()
{
  QString sWindowTitle = " MdtElmb [" + gsVersion + "]";

  if (_bConfDbStat == true)
    {
      QString sConfDb;
      sConfDb = QString::asprintf(", %s@%s", _pConfDb->getUsername().c_str(),
                                  _pConfDb->getDatabase().c_str());
      sWindowTitle += sConfDb;
    }
  emit sigWindowTitle(sWindowTitle);
}

void kernel::wrIniUsedFile(const std::string& sIniFile)
{
  std::string sIniUsedFile = sIniFile + ".used";

  FILE *fp;
  fp = fopen(sIniUsedFile.c_str(), "w");
  if (fp == NULL)
    return;

  fprintf(fp, "#\n# INI_FILE =  %s\n#\n", SCANBUS_INI_FILE);
  fprintf(fp, "[%s]\n", SETTING_SECTION);
  fprintf(fp, "%-14s= %s\n", FIRMWARE,     _sDfltFirmware.c_str());
  fprintf(fp, "%-14s= %d\n", LIFEGUARDING, _dDfltLifeGuarding);
  fprintf(fp, "\n");
  fclose(fp);

  if (_bConfDbStat == true)
    {
      if (_pConfDb->wrIniUsedFile(sIniUsedFile) == false)
        {
          QString sInfo;
          sInfo = QString::asprintf("pConfDb: cannot write ini file %s",
                                    sIniUsedFile.c_str());
          log(sInfo);
        }
    }
}

void kernel::doScan()
{
  QString sInfo;

  emit sigNodeInit();

  _nNodes = _pCanInterface->scanBus();

  for(int i = 0; i < _nNodes; ++i)
    {
      emit sigNodeAdd(_pCanInterface->scannedNodeId(i));
    }
  sInfo = QString::asprintf("#nodes: %d", _nNodes);
  log(sInfo);

  if (_nNodes > 0)
    emit sigNodeEnd();
}

void kernel::doReadNode(const int iNode)
{
  QString sInfo;

  _iNode = -1;

  if (_nNodes <= 0)
    {
      log("FATAL: no nodes scanned");
      return;
    }

  for(int i = 0; i < _nNodes; ++i)
    {
      if (iNode == _pCanInterface->scannedNodeId(i))
        {
          std::string  sChamber = "?";
          int          iSerial  = _pCanInterface->scannedSerialNr(i);
          std::string  sSerial  = convertSerial(iSerial);
          std::string  sVersion = _pCanInterface->scannedSwVersion(i);

          bool         bFound;
          if (_pConfDb->getChamber(sSerial, bFound, sChamber) == false)
            {
              sChamber = "?";
              sInfo = QString("ERROR: getChamber( %1 ) failed").arg(sSerial.c_str());
              log(sInfo);
            }
          else if (bFound == false)
            {
              sChamber = "?";
              sInfo = QString("ERROR: serial %1 not in ConfDb").arg(sSerial.c_str());
              log(sInfo);
            }

          emit sigChamber(sChamber.c_str());
          sInfo = QString("[ id: %1,  serial: %2,  version %3 ]").arg(iNode).
            arg(sSerial.c_str()).arg(sVersion.c_str());
          emit sigBaseInfo(sInfo);
          readSettings(iNode);
          return;
        }
    }
  sInfo = QString("FATAL: node %1 not in internal list").arg(iNode);
  log(sInfo);
}

void kernel::readSettings(const int iNode)
{
  int     iResult;
  QString sInfo;

  _bReadSettingsFailed = true;

  if (_pCanNode->setNodeId(iNode) == false)
    { 
      sInfo = QString("ERROR: setNodeId( %1 ) failed").arg(iNode);
      log(sInfo);
      return;
    }

  //
  // General parameters
  //
  if (readSdoObject(iNode, OD_LIFE_GUARDING, 0, 200, iResult) == false)
    return;
  _dRdLifeGuard = iResult;

  if (readSdoObject(iNode, OD_TXPDO1, 2, 200, iResult) == false)
    return;
  _bRdTxPDO1 = (iResult == 1 ? true : false);
  if (readSdoObject(iNode, OD_TXPDO2, 2, 200, iResult) == false)
    return;
  _bRdTxPDO2 = (iResult == 1 ? true : false);
  if (readSdoObject(iNode, OD_TXPDO3, 2, 200, iResult) == false)
    return;
  _bRdTxPDO3 = (iResult == 1 ? true : false);
  if (readSdoObject(iNode, OD_TXPDO4, 2, 200, iResult) == false)
    return;
  _bRdTxPDO4 = (iResult == 1 ? true : false);

  //
  // Elmb ADC parameters
  //
  if (readSdoObject(iNode, OD_ELMB_ADC, SI_CHANNELS, 200, iResult) == false)
    return;
  _dRdAdcChan = iResult;
  if (readSdoObject(iNode, OD_ELMB_ADC, SI_RATE,     200, iResult) == false)
    return;
  _dRdAdcRate = iResult;
  if (readSdoObject(iNode, OD_ELMB_ADC, SI_RANGE,    200, iResult) == false)
    return;
  _dRdAdcRange = iResult;
  if (readSdoObject(iNode, OD_ELMB_ADC, SI_MODE,     200, iResult) == false)
    return;
  _dRdAdcMode = iResult;

  if (readSdoObject(iNode, OD_ELMB_ADC_NTCCONV, 0, 200, iResult) == false)
    return;
  _dRdNtcConv = iResult;

  if (readSdoObject(iNode, OD_ELMB_ADC_ENABLED, 0, 200, iResult) == false)
    return;
  _bRdAdcEnabled = (iResult == 1 ? true : false);

  if (readSdoObject(iNode, OD_ELMB_ADC_RAWDATA, 0, 200, iResult) == false)
    return;
  _bRdRawData = (iResult == 1 ? true : false);

  //
  // Csm ADC parameters
  //
  if (readSdoObject(iNode, OD_CSM_ADC, SI_CHANNELS, 200, iResult) == false)
    return;
  _dRdCsmChan = iResult;
  if (readSdoObject(iNode, OD_CSM_ADC, SI_RATE,     200, iResult) == false)
    return;
  _dRdCsmRate = iResult;
  if (readSdoObject(iNode, OD_CSM_ADC, SI_RANGE,    200, iResult) == false)
    return;
  _dRdCsmRange = iResult;
  if (readSdoObject(iNode, OD_CSM_ADC, SI_MODE,     200, iResult) == false)
    return;
  _dRdCsmMode = iResult;
  if (readSdoObject(iNode, OD_CSM_ADC, SI_REFCHAN,  200, iResult) == false)
    return;
  _dRdRefChan = iResult;
  if (readSdoObject(iNode, OD_CSM_ADC, SI_MEZZMASK, 200, iResult) == false)
    return;
  _dRdMezzMask = iResult;

  if (readSdoObject(iNode, OD_CSM_ADC_ENABLED, 0, 200, iResult) == false)
    return;
  _bRdCsmEnabled = (iResult == 1 ? true : false);
  if (readSdoObject(iNode, OD_CSM_ADC_CALIBRATE, 0, 200, iResult) == false)
    return;
  _bRdCsmCalibrate = (iResult == 1 ? true : false);

  //
  // Bfield parameters
  //
  if (readSdoObject(iNode, OD_BF_NSENSORS, 0, 200, iResult) == false)
    return;
  _dRdBmask = iResult;
  if (readSdoObject(iNode, OD_BF_ADC_CALIBRATE, 0, 200, iResult) == false)
    return;
  _bRdBfCalibrate = (iResult == 1 ? true : false);

  //
  // DigIO parameters
  //
  if (readSdoObject(iNode, OD_DIGIO_ENABLED, 0, 200, iResult) == false)
    return;
  _bRdDigIOEnabled = (iResult == 1 ? true : false);
  if (readSdoObject(iNode, OD_INIT_DIG_OUTPUT, 0, 200, iResult) == false)
    return;
  _dRdDigOutput = iResult;

  int digio_mask = 0;
  _bRdDigBit1 = _bRdDigBit2 = _bRdDigBit3 = _bRdDigBit4 = _bRdDigBit5 = _bRdDigBit6 = _bRdDigBit7 = false;
  if (readSdoObject(iNode, OD_DIGIO_INPUT, 1, 200, iResult) == false)
    return;
  digio_mask += iResult;
  if (readSdoObject(iNode, OD_DIGIO_OUTPUT, 1, 200, iResult) == false)
    return;
  digio_mask += iResult;
  if ((digio_mask & 0x1) != 0)
    _bRdDigBit1 = true;
  if ((digio_mask & 0x2) != 0)
    _bRdDigBit2 = true;
  if ((digio_mask & 0x4) != 0)
    _bRdDigBit3 = true;
  if ((digio_mask & 0x8) != 0)
    _bRdDigBit4 = true;
  if ((digio_mask & 0x10) != 0)
    _bRdDigBit5 = true;
  if ((digio_mask & 0x20) != 0)
    _bRdDigBit6 = true;
  if ((digio_mask & 0x40) != 0)
    _bRdDigBit7 = true;

  emit sigTxPDO(_dRdLifeGuard, _bRdTxPDO1, _bRdTxPDO2, _bRdTxPDO3, _bRdTxPDO4);
  emit sigElmbAdc(_dRdAdcChan, _dRdAdcRate, _dRdAdcRange, _dRdAdcMode, _dRdNtcConv,
                  _bRdAdcEnabled, _bRdRawData);
  emit sigCsmAdc(_dRdCsmChan, _dRdCsmRate, _dRdCsmRange, _dRdCsmMode, _dRdRefChan,
                 _bRdCsmEnabled, _bRdCsmCalibrate, _dRdMezzMask);
  emit sigBfield(_dRdBmask, _bRdBfCalibrate);
  emit sigDigIO(_bRdDigIOEnabled, _dRdDigOutput, _bRdDigBit1, _bRdDigBit2, _bRdDigBit3, _bRdDigBit4,
                _bRdDigBit5, _bRdDigBit6, _bRdDigBit7);
  emit sigSettingWidgets(true);

  _bReadSettingsFailed = false;
  _iNode = iNode;  // to be used eventually by upd_WriteSave
}

void kernel::pSeriousError(const QString& sInfo)
{
  QString sMess;

  sMess  = "  *ERROR*: ";
  sMess += sInfo;
  log(sMess);
}

bool kernel::readSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                           const unsigned int iTimeout, int& iResult)
{
  int     no_of_bytes;
  QString sInfo;

  if (iNode != _pCanNode->nodeId())
    {
      sInfo = QString::asprintf("readSdoObject( node=%d not valid, should be %d )",
                                iNode, _pCanNode->nodeId());
      pSeriousError(sInfo);
      return false;
    }

  if (_pCanNode->sdoReadExpedited(iIndex, iSubIndex, &no_of_bytes, &iResult, iTimeout) == false)
    {
      sInfo = QString::asprintf("sdoReadExpedited( node=%d, index=x%X, subindex=x%X ) failed",
                                iNode, iIndex, iSubIndex);
      pSeriousError(sInfo);
      return false;
    }
  return true;
}

bool kernel::writeSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                            const int nBytes, const unsigned int iTimeout, int iData)
{
  QString sInfo;

  if (iNode != _pCanNode->nodeId())
    {
      sInfo = QString::asprintf("writeSdoObject( node=%d not valid, should be %d )",
                                iNode, _pCanNode->nodeId());
      pSeriousError(sInfo);
      return false;
    }

  if (_pCanNode->sdoWriteExpedited(iIndex, iSubIndex, nBytes, iData, iTimeout) == false)
    {
      sInfo = QString::asprintf("sdoWriteExpedited( node=%d, index=x%X, subindex=x%X, nbytes=%d, data=x%X ) failed",
                                iNode, iIndex, iSubIndex, nBytes, iData);
      pSeriousError(sInfo);
      return false;
    }
  return true;
}

bool kernel::writeSave(const int iNode)
{
  QString sInfo;

  if (iNode != _pCanNode->nodeId())
    {
      sInfo = QString::asprintf("writeSave( node=%d not valid, should be %d )",
                                iNode, _pCanNode->nodeId());
      pSeriousError(sInfo);
      return false;
    }

  int iIndex    = OD_EEPROM;
  int iSubIndex = 1;
  int nBytes    = 4;

  unsigned char  data[4];
  data[0] = 's';
  data[1] = 'a';
  data[2] = 'v';
  data[3] = 'e';

  if (_pCanNode->sdoWriteExpedited(iIndex, iSubIndex, nBytes, data, 2000) == false)
    {
      sInfo = QString::asprintf("sdoWriteExpedited( node=%d, index=x%X, subindex=x%X, nbytes=%d ) failed",
                                iNode, iIndex, iSubIndex, nBytes);
      pSeriousError(sInfo);
      return false;
    }
  return true;
}

std::string kernel::convertSerial(const int iSerial)
{
  char        cBuf[BUF_SIZE];
  std::string sSerial;

  char c0 = (char)(iSerial & 0x7f);
  char c1 = (char)((iSerial & 0x7f00) >> 8);
  char c2 = (char)((iSerial & 0x7f0000) >> 16);
  char c3 = (char)((iSerial & 0x7f000000) >> 24);

  sprintf(cBuf, "%c%c%c%c", c0,c1,c2,c3);
  sSerial = cBuf;

  return sSerial;
}
