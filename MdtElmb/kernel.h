/*
 *  kernel.h ------
 *  
 */

#ifndef KERNEL_H
#define KERNEL_H

#include <QThread>
#include <QMessageBox>

// forward declarations
class MdtElmbDialog;
class CanInterface;
class CanNode;
class confDb;

class kernel: public QThread
{
    Q_OBJECT

public:
                        kernel(MdtElmbDialog* pDialog);
                       ~kernel();

    void                run();
    bool                initKernel();
    bool                getStatus()  { return _bStatus; };

private:
    bool                _bStatus;
    MdtElmbDialog*      _pMdtElmbDialog;
    CanInterface*       _pCanInterface;
    CanNode*            _pCanNode;
    confDb*             _pConfDb;
    bool                _bConfDbStat;
    std::string         _sHostname;
    std::string         _sDfltFirmware;
    int                 _dDfltLifeGuarding;
    int                 _iPortCount;
    QString             _sInterfaceType;
    int                 _dInterfaceType;
    int                 _iNode;
    int                 _iPortNo;
    int                 _iBaudrate;
    int                 _nNodes;
    bool                _bReadSettingsFailed;

    int                 _dRdLifeGuard;
    bool                _bRdTxPDO1, _bRdTxPDO2, _bRdTxPDO3, _bRdTxPDO4;
    int                 _dRdAdcChan, _dRdAdcRate, _dRdAdcRange, _dRdAdcMode, _dRdNtcConv;
    bool                _bRdAdcEnabled, _bRdRawData;
    int                 _dRdCsmChan, _dRdRefChan, _dRdCsmRate, _dRdCsmRange, _dRdCsmMode;
    bool                _bRdCsmEnabled, _bRdCsmCalibrate;
    int                 _dRdMezzMask;
    int                 _dRdBmask;
    bool                _bRdBfCalibrate;
    bool                _bRdDigIOEnabled;
    int                 _dRdDigOutput;
    bool                _bRdDigBit1, _bRdDigBit2, _bRdDigBit3, _bRdDigBit4, _bRdDigBit5, _bRdDigBit6, _bRdDigBit7;

    void                cleanUp();
    void                log(const QString& sMess);
    void                setWindowTitle();
    void                wrIniUsedFile(const std::string& sIniFile);

    void                openCloseCanPort(const bool bReportError, const QString& sInterfaceType,
                                         const int dInterfaceType, const int iPortNo, const int iBaudrate);
    void		doScan();
    void		doReadNode(const int iNode);
    void                readSettings(const int iNode);
    void                pSeriousError(const QString& sInfo);
    std::string         convertSerial(const int iSerial);
    bool                readSdoObject(const int iNode, const int iIndex, const int iSubIndex,
                                      const unsigned int iTimeout, int &dResult);
    bool                writeSdoObject(const int iNode, const int iIndex, const int iSubIndex, const int nBytes,
                                       const unsigned int iTimeout, const int iData);
    bool                writeSave(const int iNode);

private slots:

public  slots:
    void                upd_OpenPort(const QString& sInterfaceType, const int dInterfaceTupe, const int iPortNo, const int iBaudrate);
    void                upd_PortCount(const int iPortCount);
    void                upd_Scan();
    void                upd_ReadNode(const int iNode);
    void                upd_WriteSave(const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4,
                                      const int nAdcChan, const int dAdcRate, const int dAdcRange, const int dAdcMode,
                                      const int dNtcConv, const bool bAdcEnabled, const bool bRawData,
                                      const int nCsmChan, const int dCsmRate, const int dCsmRange, const int dCsmMode,
                                      const int dRefChan, const bool bCsmEnabled, const bool bCsmCalibrate, const int dMezzMask,
                                      const int dBmask, const bool bBfCalibrate,
                                      const bool bDigIOEnabled, const int dDigOutput,
                                      const bool bDigBit1, const bool bDigBit2, const bool bDigBit3, const bool bDigBit4);

signals:
    void                sigLogEdit(const QString& sMess);
    void                sigOpenPortStatus(const bool bOpen);
    void                sigWindowTitle(const QString& sTitle);
    void                sigCanBus(const QString& sCanBus);
    void                sigChamber(const QString& sChamber);
    void                sigBaseInfo(const QString& sBaseInfo);
    void                sigNodeInit();
    void                sigNodeAdd(const int iNode);
    void                sigNodeEnd();
    void                sigTxPDO(const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4);
    void                sigElmbAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dNtcConv, const bool bEnabled, const bool bRawData);
    void                sigCsmAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dRefChan,
                                  const bool bEnabled, const bool bCalibrate, const int dMezzMask);
    void                sigDigIO(const bool bDigIOEnabled, const int dDigOutput, const bool bDigBit1, const bool bDigBit2,
                                 const bool bDigBit3, const bool bDigBit4, const bool bDigBit5, const bool bDigBit6, const bool bDigBit7);
    void                sigBfield(const int dBmask, const bool bCalibrate);
    void                sigSettingWidgets(const bool bEnable);
};

#endif
