/*
 *  MdtElmbDialog.cpp ------
 *  The GUI part of the MdtElmb application.
 *  Author: Robert.Hart@nikhef.nl
 *  September 2016, moved to Linux SLC6
 */

#include <QFile>
#include <QImage>
#include <QLabel>
#include <QMessageBox>
#include <QTextEdit>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QDateTime>

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif // WIN32
#include <time.h>

#include "MdtElmbDialog.h"
#include "CanInterface.h"
#include "kernel.h"
#include "mdt_obj_dict.h"

// Setting variables and defaults
const   QString gsOrganization( "Nikhef"    );
const   QString gsApplication ( "MdtElmb"   );
const   QString gsVarIntfType ( "intfType"  );
const   QString gsVarPortIndex( "portIndex" );
const   QString gsVarBaudrate ( "baudrate"  );
// The defaults
const   QString gsDfltIntfType( "Socket"    );
const   int     giDfltPortIndex = 0;
const   int     giDfltBaudrate  = 125;

bool    gbStopThread;

// -----------------------------------------------------------------------------

// Constructor
MdtElmbDialog::MdtElmbDialog() : QDialog()
{
  setupUi( this );

  gbStopThread = false;

  Qt::WindowFlags flags = windowFlags();
  flags |= Qt::MSWindowsFixedSizeDialogHint;
  flags |= Qt::WindowMinimizeButtonHint;
  setWindowFlags(flags);

  cbInterface->clear();
  cbInterface->addItem( "KVASER" );
#ifdef WIN32
  cbInterface->addItem( "SYSTEC" );
  cbInterface->addItem( "PEAK"   );
  cbInterface->addItem( "NICAN"  );
#else
  cbInterface->addItem( "Socket" );
#endif // WIN32

  int     i;
  QString sItem;

  cbBaudrate->clear();
  cbBaudrate->addItem( QString::number(10) );
  cbBaudrate->addItem( QString::number(20) );
  cbBaudrate->addItem( QString::number(50) );
  cbBaudrate->addItem( QString::number(125) ); // default
  cbBaudrate->addItem( QString::number(250) );
  cbBaudrate->addItem( QString::number(500) );
  cbBaudrate->addItem( QString::number(800) );
  cbBaudrate->addItem( QString::number(1000) );

  cbElmbRate->clear();
  cbCsmRate->clear();
  sItem = "15.0 Hz (0)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "30.0 Hz (1)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "61.6 Hz (2)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "84.5 Hz (3)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "101.1 Hz (4)"; cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "1.88 Hz (5)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "3.76 Hz (6)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);
  sItem = "7.51 Hz (7)";  cbElmbRate->addItem(sItem);  cbCsmRate->addItem(sItem);

  cbElmbRange->clear();
  cbCsmRange->clear();
  sItem = "100 mV (0)";   cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);
  sItem = "55 mV (1)";    cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);
  sItem = "25 mV (2)";    cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);
  sItem = "1 V (3)";      cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);
  sItem = "5 V (4)";      cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);
  sItem = "2.5 V (5)";    cbElmbRange->addItem(sItem); cbCsmRange->addItem(sItem);

  cbElmbMode->clear();
  cbCsmMode->clear();
  sItem = "Bipolar (0)";  cbElmbMode->addItem(sItem);  cbCsmMode->addItem(sItem);
  sItem = "Unipolar(1)";  cbElmbMode->addItem(sItem);  cbCsmMode->addItem(sItem);

  cbElmbChannels->clear();
  cbCsmChannels->clear();
  cbCsmRefChan->clear();
  for(i = 0; i <= 64; ++i)
    {
      sItem = QString::asprintf("%d", i);
      cbElmbChannels->addItem(sItem);
      cbCsmChannels->addItem(sItem);
      cbCsmRefChan->addItem(sItem);
    }

  cbBfieldMask->clear();
  cbDigInitOutput->clear();
  for(i = 0; i < 16; ++i)
    {
      sItem = QString::asprintf("x%02X", i);
      cbBfieldMask->addItem(sItem);
      cbDigInitOutput->addItem(sItem);
    }

  this->readAppSettings();

  connect(butQuit,      SIGNAL(clicked()),                           this, SLOT(onQuit()));
  connect(butOpenPort,  SIGNAL(clicked()),                           this, SLOT(onOpenPort()));
  connect(butScan,      SIGNAL(clicked()),                           this, SLOT(onScan()));
  connect(butReadNode,  SIGNAL(clicked()),                           this, SLOT(onReadNode()));
  connect(butWriteSave, SIGNAL(clicked()),                           this, SLOT(onWriteSave()));
  connect(cbInterface,  SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onInterface(const QString&)));
  connect(cbNode,       SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onNode(const QString&)));

  _pKernel = new kernel(this);

  connect(_pKernel, SIGNAL(sigLogEdit(const QString&)),     this, SLOT(upd_Stdout(const QString&)));
  connect(_pKernel, SIGNAL(sigOpenPortStatus(const bool)),  this, SLOT(upd_OpenPortStatus(const bool)));
  connect(_pKernel, SIGNAL(sigWindowTitle(const QString&)), this, SLOT(upd_WindowTitle(const QString&)));
  connect(_pKernel, SIGNAL(sigCanBus(const QString&)),      this, SLOT(upd_CanBus(const QString&)));
  connect(_pKernel, SIGNAL(sigChamber(const QString&)),     this, SLOT(upd_Chamber(const QString&)));
  connect(_pKernel, SIGNAL(sigBaseInfo(const QString&)),    this, SLOT(upd_BaseInfo(const QString&)));
  connect(_pKernel, SIGNAL(sigNodeInit()),                  this, SLOT(upd_NodeInit()));
  connect(_pKernel, SIGNAL(sigNodeAdd(const int)),          this, SLOT(upd_NodeAdd(const int)));
  connect(_pKernel, SIGNAL(sigNodeEnd()),                   this, SLOT(upd_NodeEnd()));
  connect(_pKernel, SIGNAL(sigTxPDO(const int, const bool, const bool, const bool, const bool)), this,
          SLOT(upd_TxPDO (const int, const bool, const bool, const bool, const bool)));
  connect(_pKernel, SIGNAL(sigElmbAdc(const int, const int, const int, const int, const int, const bool, const bool)), this,
          SLOT(upd_ElmbAdc (const int, const int, const int, const int, const int, const bool, const bool)));
  connect(_pKernel, SIGNAL(sigCsmAdc (const int, const int, const int, const int, const int, const bool, const bool, const int)), this,
          SLOT(upd_CsmAdc  (const int, const int, const int, const int, const int, const bool, const bool, const int)));
  connect(_pKernel, SIGNAL(sigBfield (const int, const bool)), this, SLOT(upd_Bfield(const int, const bool)));
  connect(_pKernel, SIGNAL(sigDigIO(const bool, const int, const bool, const bool, const bool, const bool, const bool, const bool, const bool)), this,
          SLOT(upd_DigIO (const bool, const int, const bool, const bool, const bool, const bool, const bool, const bool, const bool)));
  connect(_pKernel, SIGNAL(sigSettingWidgets(const bool)),  this, SLOT(upd_SettingWidgets(const bool)));

  upd_CanBus("<not conn>");
  upd_Chamber("");
  upd_BaseInfo("");
  upd_OpenPortStatus(true);
  upd_NodeInit();
  upd_SettingWidgets(false);

  if (_pKernel->initKernel() == false)
    {
      upd_Stdout("FATAL: initKernel() failed");
      return;
    }

  _pKernel->start();

  emit sigPortCount(cbPort->count());
}

// Destructor
MdtElmbDialog::~MdtElmbDialog()
{
  this->cleanUp();
}

void MdtElmbDialog::onQuit()
{
  this->cleanUp();
  this->writeAppSettings();
  exit(0);
}

void MdtElmbDialog::cleanUp()
{
  gbStopThread = true;
  if (_pKernel->getStatus() == true)
    {
      _pKernel->wait();
      delete _pKernel;
    }
}

void MdtElmbDialog::onOpenPort()
{
  QString sInterface = cbInterface->currentText();
  int     iPortNo    = cbPort->currentText().toInt();
  int     iBaudrate  = cbBaudrate->currentText().toInt();

  emit sigOpenPort(sInterface, _dInterfaceType, iPortNo, iBaudrate);
}

void MdtElmbDialog::onInterface(const QString& sIntfType)
{
  cbPort->clear();  // clear the port combobox

  _dInterfaceType = -1;
  if (sIntfType == QString("KVASER"))
    _dInterfaceType = KVASER_INTF_TYPE;
  else if (sIntfType == QString("SYSTEC"))
    _dInterfaceType = SYSTEC_INTF_TYPE;
  else if (sIntfType == QString("PEAK"))
    _dInterfaceType = PEAK_INTF_TYPE;
  else if (sIntfType == QString("NICAN"))
    _dInterfaceType = NICAN_INTF_TYPE;
  else if (sIntfType == QString("Socket"))
    _dInterfaceType = SOCKET_INTF_TYPE;

  // determine the number of ports available of the seclected type
  int  no_of_ports  = 0;
  int* port_numbers = 0;

  no_of_ports = CanInterface::canInterfacePorts(_dInterfaceType, &port_numbers);

  if (_dInterfaceType < 0)
    no_of_ports = 0;

  // fill the Port combobox
  for(int i = 0; i < no_of_ports; ++i)
    this->cbPort->addItem( QString::number(port_numbers[i]) );

  bool  bEnabled = (no_of_ports > 0 ? true : false);
  butOpenPort->setEnabled(bEnabled);
  cbBaudrate->setEnabled(bEnabled);
}

void MdtElmbDialog::onNode(const QString& sNode)
{
  QString sFool = sNode; // fools the compiler, no warning anymore
  upd_SettingWidgets(false);
}

void MdtElmbDialog::onScan()
{
  emit sigScan();
}

void MdtElmbDialog::onReadNode()
{
  int iNode = cbNode->currentText().toInt();
  emit sigReadNode(iNode);
}

void MdtElmbDialog::onWriteSave()
{
  QString     sInfo;
  std::string sString;

  int  dLifeGuard = leLifeGuard->text().toInt();
  bool bTxPDO1    = cbTxPDO1->isChecked();
  bool bTxPDO2    = cbTxPDO2->isChecked();
  bool bTxPDO3    = cbTxPDO3->isChecked();
  bool bTxPDO4    = cbTxPDO4->isChecked();

  int  nAdcChan    = cbElmbChannels->currentIndex();
  int  dAdcRate    = cbElmbRate->currentIndex();
  int  dAdcRange   = cbElmbRange->currentIndex();
  int  dAdcMode    = cbElmbMode->currentIndex();
  bool bAdcEnabled = cbElmbEnabled->isChecked();
  bool bRawData    = cbElmbRawData->isChecked();
  int  dNtcConv    = -1;
  if (rbResistor->isChecked())
    dNtcConv = NTC_RESISTOR;
  else if (rbMdegrees->isChecked())
    dNtcConv = NTC_MDEGREES;

  int  nCsmChan      = cbCsmChannels->currentIndex();
  int  dCsmRate      = cbCsmRate->currentIndex();
  int  dCsmRange     = cbCsmRange->currentIndex();
  int  dCsmMode      = cbCsmMode->currentIndex();
  int  dRefChan      = cbCsmRefChan->currentIndex();
  bool bCsmEnabled   = cbCsmEnabled->isChecked();
  bool bCsmCalibrate = cbCsmCalibrate->isChecked();
  QString sMezzMask  = leMezzMask->text();
  sString = sMezzMask.toUtf8().constData();
  int dMezzMask;
  int n = sscanf(sString.c_str(), "x%x", &dMezzMask);
  if (n != 1)
    dMezzMask = -1;

  int  dBmask        = cbBfieldMask->currentIndex();
  bool bBfCalibrate  = cbBsensorCalibrate->isChecked();

  bool bDigIOEnabled = cbDigIOEnabled->isChecked();
  int  dDigOutput    = cbDigInitOutput->currentIndex();
  bool bDigBit1      = cbDigBit1->isChecked();
  bool bDigBit2      = cbDigBit2->isChecked();
  bool bDigBit3      = cbDigBit3->isChecked();
  bool bDigBit4      = cbDigBit4->isChecked();

  emit sigWriteSave(dLifeGuard, bTxPDO1, bTxPDO2, bTxPDO3, bTxPDO4,
                    nAdcChan, dAdcRate, dAdcRange, dAdcMode, dNtcConv, bAdcEnabled, bRawData,
                    nCsmChan, dCsmRate, dCsmRange, dCsmMode, dRefChan, bCsmEnabled, bCsmCalibrate, dMezzMask,
                    dBmask, bBfCalibrate,
                    bDigIOEnabled, dDigOutput, bDigBit1, bDigBit2, bDigBit3, bDigBit4);
}

void MdtElmbDialog::upd_Stdout(const QString& sText)
{
  logStdout->append(sText);
  logStdout->ensureCursorVisible();
  QCoreApplication::processEvents();
}

void::MdtElmbDialog::upd_OpenPortStatus(const bool bOpen)
{
  upd_NodeInit();
  cbNode->setEnabled(false);
  butReadNode->setEnabled(false);

  if (bOpen == true)
    {
      butOpenPort->setText("Open");
      butScan->setEnabled(false);
    }
  else
    {
      butOpenPort->setText("Close");
      butScan->setEnabled(true);
    }

  cbInterface->setEnabled(bOpen);
  cbPort->setEnabled(bOpen);
  cbBaudrate->setEnabled(bOpen);
}

void::MdtElmbDialog::upd_WindowTitle(const QString& sTitle)
{
  setWindowTitle(sTitle);
}

void MdtElmbDialog::upd_CanBus(const QString& sCanBus)
{
  labCanBus->setText(sCanBus);
}

void MdtElmbDialog::upd_Chamber(const QString& sChamber)
{
  labChamber->setText(sChamber);
}

void MdtElmbDialog::upd_BaseInfo(const QString& sBaseInfo)
{
  labBaseInfo->setText(sBaseInfo);
}

void MdtElmbDialog::upd_NodeInit()
{
  cbNode->clear();
}

void MdtElmbDialog::upd_NodeAdd(const int iNode)
{
  cbNode->addItem( QString::number(iNode) );
}

void MdtElmbDialog::upd_NodeEnd()
{
  butScan->setEnabled(false);
  cbNode->setEnabled(true);
  butReadNode->setEnabled(true);
}

void MdtElmbDialog::upd_TxPDO(const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4)
{
  QString sInfo;
  sInfo = QString::asprintf("%d", dLifeGuard);
  leLifeGuard->setText(sInfo);

  cbTxPDO1->setChecked(bTxPDO1);
  cbTxPDO2->setChecked(bTxPDO2);
  cbTxPDO3->setChecked(bTxPDO3);
  cbTxPDO4->setChecked(bTxPDO4);
}

void MdtElmbDialog::upd_ElmbAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dNtcConv, const bool bEnabled, const bool bRawData)
{
  cbElmbChannels->setCurrentIndex(nChan);
  cbElmbRate->setCurrentIndex(dRate);
  cbElmbRange->setCurrentIndex(dRange);
  cbElmbMode->setCurrentIndex(dMode);
  cbElmbEnabled->setChecked(bEnabled);
  cbElmbRawData->setChecked(bRawData);

  switch(dNtcConv)
    {
    case NTC_RESISTOR:
      rbResistor->setChecked(true);
      break;
    case NTC_MDEGREES:
    default:
      rbMdegrees->setChecked(true);
      break;
    }
}

void MdtElmbDialog::upd_CsmAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dRefChan, const bool bEnabled, const bool bCalibrate, const int dMezzMask)
{
  QString sInfo;
  sInfo = QString::asprintf("x%X", dMezzMask);
  leMezzMask->setText(sInfo);

  cbCsmChannels->setCurrentIndex(nChan);
  cbCsmRate->setCurrentIndex(dRate);
  cbCsmRange->setCurrentIndex(dRange);
  cbCsmMode->setCurrentIndex(dMode);
  cbCsmRefChan->setCurrentIndex(dRefChan);
  cbCsmEnabled->setChecked(bEnabled);
  cbCsmCalibrate->setChecked(bCalibrate);
}

void MdtElmbDialog::upd_Bfield(const int dBmask, const bool bCalibrate)
{
  cbBfieldMask->setCurrentIndex(dBmask);
  cbBsensorCalibrate->setChecked(bCalibrate);
}

void MdtElmbDialog::upd_DigIO(const bool bDigIOEnabled, const int dDigOutput, const bool bDigBit1, const bool bDigBit2,
                              const bool bDigBit3, const bool bDigBit4, const bool bDigBit5, const bool bDigBit6, const bool bDigBit7)
{
  cbDigIOEnabled->setChecked(bDigIOEnabled);
  cbDigInitOutput->setCurrentIndex(dDigOutput);
  cbDigBit1->setChecked(bDigBit1);
  cbDigBit2->setChecked(bDigBit2);
  cbDigBit3->setChecked(bDigBit3);
  cbDigBit4->setChecked(bDigBit4);
  cbDigBit5->setChecked(bDigBit5);
  cbDigBit6->setChecked(bDigBit6);
  cbDigBit7->setChecked(bDigBit7);
}

void MdtElmbDialog::upd_SettingWidgets(const bool bEnable)
{
  gbChamber->setEnabled(bEnable);

  // the DigIO input (status) checkboxes remain inactive
  cbDigBit5->setEnabled(false);
  cbDigBit6->setEnabled(false);
  cbDigBit7->setEnabled(false);

  if (bEnable == false)
    {
      upd_TxPDO(0, false, false, false, false);
      upd_ElmbAdc(0, 0, 0, 0, -1, false, false);
      upd_CsmAdc(0, 0, 0, 0, 0, false, false, 0);
      upd_Bfield(0, false);
      upd_DigIO(false, 0, false, false, false, false, false, false, false);
    }
}

void MdtElmbDialog::readAppSettings()
{
  int         iIndex;
  int         iPort;
  int         iBaudrate;
  QString     sIntfType;
  QString     sBaudrate;
  QSettings   settings( gsOrganization, gsApplication );

  sIntfType = settings.value( gsVarIntfType, gsDfltIntfType ).toString();
  iIndex = cbInterface->findText( sIntfType );
  cbInterface->setCurrentIndex( iIndex );
  // call onInterface in order to fill the Port combobox
  this->onInterface( sIntfType );

  iPort = settings.value( gsVarPortIndex, giDfltPortIndex ).toInt();
  if (iPort >= 0 && iPort < cbPort->count())
    cbPort->setCurrentIndex( iPort );

  iBaudrate = settings.value( gsVarBaudrate, giDfltBaudrate ).toInt();
  sBaudrate = QString::number( iBaudrate );
  iIndex = cbBaudrate->findText( sBaudrate );
  cbBaudrate->setCurrentIndex( iIndex );
}

void MdtElmbDialog::writeAppSettings()
{
  int         iPort;
  QSettings   settings( gsOrganization, gsApplication );

  settings.setValue( gsVarIntfType, cbInterface->currentText());
  iPort = cbPort->currentIndex();
  if (iPort >= 0)
    settings.setValue( gsVarPortIndex, iPort);
  settings.setValue( gsVarBaudrate, cbBaudrate->currentText().toInt() );
}
