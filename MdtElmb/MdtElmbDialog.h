/*
 *  MdtElmbDialog.h ------
 */

#ifndef MDTELMB_H
#define MDTELMB_H

#include <QDialog>

#include "ui_MdtElmb.h"

// Henk: huh? what's this?:
//#ifdef UNICODE
//#error UNICODE set!
//#endif

// forward declarations
class kernel;

class MdtElmbDialog: public QDialog, public Ui_Dialog
{
    Q_OBJECT

public:
                MdtElmbDialog();
               ~MdtElmbDialog();
private:
    kernel*     _pKernel;
    int         _dInterfaceType;

    void        cleanUp();
    void        readAppSettings();
    void        writeAppSettings();

private slots:
    void        onQuit();
    void        onOpenPort();
    void        onInterface(const QString &sText);
    void        onNode(const QString &sText);
    void        onScan();
    void        onReadNode();
    void        onWriteSave();

public slots:
    void        upd_Stdout(const QString& sText);
    void        upd_OpenPortStatus(const bool bOpen);
    void        upd_WindowTitle(const QString& sTitle);
    void	upd_CanBus(const QString& sCanBus);
    void	upd_Chamber(const QString& sChamber);
    void	upd_BaseInfo(const QString& sBaseInfo);
    void        upd_NodeInit();
    void        upd_NodeAdd(const int iNode);
    void        upd_NodeEnd();
    void	upd_TxPDO(const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4);
    void        upd_ElmbAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dNtcConv,
                            const bool bEnabled, const bool bRawData);
    void        upd_CsmAdc(const int nChan, const int dRate, const int dRange, const int dMode, const int dRefChan,
                           const bool bEnabled, const bool bCalibrate, const int dMezzMask);
    void        upd_Bfield(const int dBmask, const bool bCalibrate);
    void        upd_DigIO(const bool bDigIOEnabled, const int dDigOutput, const bool bDigBit1, const bool bDigBit2,
                          const bool bDigBit3, const bool bDigBit4, const bool bDigBit5, const bool bDigBit6, const bool bDigBit7);
    void        upd_SettingWidgets(const bool bEnable);

signals:
    void        sigOpenPort(const QString& sInterfaceType, const int dInterfacetype, const int iPortNo, const int iBaudrate);
    void        sigPortCount(const int iPortCount);
    void        sigScan();
    void        sigReadNode(const int iNode);
    void	sigWriteSave(const int dLifeGuard, const bool bTxPDO1, const bool bTxPDO2, const bool bTxPDO3, const bool bTxPDO4,
                             const int nAdcChan, const int dAdcRate, const int dAdcRange, const int dAdcMode, const int dNtcConv,
                             const bool bAdcEnabled, const bool bRawData,
                             const int nCsmChan, const int dCsmRate, const int dCsmRange, const int dCsmMode, const int dRefChan,
                             const bool bCsmEnabled, const bool bCsmCalibrate, const int dMezzMask,
                             const int dBmask, const bool bBfCalibrate,
                             const bool bDigIOEnabled, const int dDigOutput,
                             const bool bDigBit1, const bool bDigBit2, const bool bDigBit3, const bool bDigBit4);
};

#endif
