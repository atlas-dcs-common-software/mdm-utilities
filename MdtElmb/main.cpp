/*
 *  main ------
 *  Starting point of the MdtElmb application.
 *  September 2016, Robert.Hart@nikhef.nl
 */

#include <QApplication>

#include "MdtElmbDialog.h"

int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );

  MdtElmbDialog mdtelmbdialog;
  mdtelmbdialog.show();
  return app.exec();
}
